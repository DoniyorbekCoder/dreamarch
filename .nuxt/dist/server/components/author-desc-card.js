exports.ids = [6];
exports.modules = {

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AuthorDescCard.vue?vue&type=template&id=6053e95a&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "w-292 border border-white-secondary border-t-2 border-t-yellow-secondary relative"
  }, [_vm._ssrNode("<div class=\"p-30 relative box\" data-v-6053e95a><p class=\"text\" data-v-6053e95a>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper pellentesque eget venenatis, est urna. Congue augue risus eu, egestas aliquam lacus morbi. Duis amet nec eu turpis ut pharetra a facilisi. Purus morbi ornare sollicitudin in porttitor sit nerus mentos sollicitudin in porttitor sit nerus mentos.</p></div> <div class=\"p-30 w-full flex items-center gap-15 bg-gray-primary\" data-v-6053e95a><div class=\"w-50 h-50 rounded-full overflow-hidden border border-white-secondary\" data-v-6053e95a><img" + _vm._ssrAttr("src", __webpack_require__(61)) + " alt=\"author\" class=\"w-full h-full object-cover object-top\" data-v-6053e95a></div> <p class=\"text-base font-semibold flex-wrap leading-24\" data-v-6053e95a>Sardorbek Rashidov</p></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AuthorDescCard.vue?vue&type=template&id=6053e95a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AuthorDescCard.vue?vue&type=script&lang=js&
/* harmony default export */ var AuthorDescCardvue_type_script_lang_js_ = ({
  name: "AuthorDescCard"
});
// CONCATENATED MODULE: ./components/AuthorDescCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AuthorDescCardvue_type_script_lang_js_ = (AuthorDescCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AuthorDescCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(83)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AuthorDescCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6053e95a",
  "e0bd8608"
  
)

/* harmony default export */ var AuthorDescCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/author.c4e7c34.jpg";

/***/ }),

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("fad7d56c", content, true, context)
};

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(70);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(20);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(85);
var ___CSS_LOADER_URL_IMPORT_1___ = __webpack_require__(86);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
var ___CSS_LOADER_URL_REPLACEMENT_1___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_1___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "*[data-v-6053e95a]{box-sizing:border-box}.text[data-v-6053e95a]{display:-webkit-box;-webkit-line-clamp:10;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis}.box[data-v-6053e95a]:after{top:15px;left:15px;background:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat 0 0;background-size:15px}.box[data-v-6053e95a]:after,.box[data-v-6053e95a]:before{content:\"\";position:absolute;width:15px;height:12px}.box[data-v-6053e95a]:before{bottom:15px;right:15px;background:url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ") no-repeat 0 0;background-size:15px}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 85:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAwCAYAAABNPhkJAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMQSURBVHgB7ZrLcRpBEIZ/RjyO9o3iVV4isB2BpQhMBsYRWI7AKAOUgRSByUDrCGxHwLqKR3GyfOONu6mFEi6xO8u/i1xov4sezNLzd/f09Axk+v3+ChxepVKpg0Dm8EV+tECwWq2a1Wr1NmycwTMjFXzqpIJPnVTwqZMKPnVSwadOKvjU+S8EZzKZexwJI8Y8PDHL5fIPSIwxL63GgccBiTj9N0jkPGwnWAb+AEm327Uytg+Zwy+QyHs4NuNUsAeSQqHwAgSz2cwDiWSJXYQFDyTitDcgqNfr9zHUEqs5aNGiU1rewwFJDEvLsRlkxuPxT5BIlaUirIjTXJD0er3QeRhNJ/GuCwKZ7DlI4sg0WZ7vQsf4xlxwOIPB4BUI/EyjGhCbTFsLFs+44GmAwM80Nsqhc1gLLpVK38B7lxLsE3qRHoRuTcPhMDCtt52WePcGnLFztgGR/bgD0vGLxSLQ8VvBZ2dnHZDk8/lPIIgprZtBjt8K1rRmq7U8fwkScXwLBJrWQY7fOTxI8aKirMZkL/wAgjjqidDc98KO4MlkcpuksQi0weHsK147gnUNsca0eIVVyjCm0+k1+OLVeuz/5pjGbEnS8eaYxqKQlOPNMY1FISnHmySN9fv99yCIw/HSAd48/NuEGPPA0Wa6L/9ioAUOPdhs92UTZEz25SZIY2z3VS6Xr9mGSKLc2jg+8NbSbwKoZkS7L/boKN3XRxCprQ1RLpfTbwqFX9NKaqsxDweixv5dR1ERx3tsasvzl1rAMjaDdaBM2gWBPH9Rq9VcEEjbesfcrujBxOoi3k/tFg5EvcuKVfzU9nAAa7HGNKwivEG2ma+IdrPhZbPZRrFYpC8KN4xGo9fz+dyVX62rv4hty1n7al2IEQF/PdueVzsy/m2cYhV9P4mU7TFUC12jWq1+9nsLRIqwIuvZkfV4h4B7YE1h3U6QIGHfz9yksMxj52OcyIKVANGxp3AQ+0RrCmtUH3vmoE8PdZsQ711gt4AkksJBVCqVK+wK3qYwkkAjrVvFw9btKVD7Mo/vNg3OX6q4gvFXKpCjAAAAAElFTkSuQmCC"

/***/ }),

/***/ 86:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAwCAYAAABNPhkJAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAALsSURBVHgB7ZpBbtNAFIb/NwlruAG5Ab0BUIgidukJGm5Qdm1UqWbRZElvQDgBYYVoJJreoD0B5gZBYgE0yfDGYFDTxB7nnxTJ+NvESmzN/O/9895MZIEHp310YPEaBCKImgd4CQKdxyedRwMEBv8ZleCyUwkuO5XgslMJLjuV4LJTCS47leClCO6CRCxi/GssJl6CdbL3QGINJmAhz8KauNgvwwEETy2+gOAs4ueggie+a7gBkm/fcQGCqzv8HKzFhZdgywrWtbMTcZaWGu6DxNUR3wxvgcFw2XXYAC6bGVzmCj49JsXCLR2MQSJzPALJswOMcwVbwQOQTIFzkMxZl8mvoOcK1hvaYND16yILgvd9NIS0dOqy/AyDs5IV3s7G4iFIUpdlCv7YSwai+l8NGIJEJ9kBR5y6LFPwVOiBXLEZg8DZmXWZ2nScXpqsgfTGDqhxMHxyiM8gqAO7INF29Ca9Xik4xLoxBgOQzG04OzuyLB2BHKi5j3cg+NDDLludzYKOOjY0kPr5BDwRGLQlir1eQ8xGBlJqlqvOrkMIv528UUNuCA6U3QFbrGZhgh4tfrcswxE2MFARXHYDtKKlQb8mOER2dWcVsdm9AlfdVcNkVdAXMxyBI67PuMmGCPpccLIq6CbkQK4FsNlFgKC3Mt4lSQQnu6oAAz3t/t3RrMPoGEeh++6S35MNPj1QjTygJ3tmwR4YtFDlBd0E2TMHKFQu6OBOZrFPdzB601sQiJ53W+T7V8kxlAy6nsr2fIJuZoId/YyxHrGZ8UfI7S7O3YtrWBPnsOah375d0otRD6+02RdaQ3WDre19XCIQbnmp485QoJ6ogGGzmyTNiz9tSR968fufhdjnQQ1OJ6RYhx7jYnXcY1Ux8Hwk/voDz1EAWfzCJ8rOQuy6zWPUx5HNLkKx6wxFi6UUHfA2xKZkBH8tsY6VfwC4N18XLX6bYh0rLL62WIfk3ZBGWdds1CJ3UgyJ4+Zo61zaTM//Ccm8Cf5AhLGaAAAAAElFTkSuQmCC"

/***/ })

};;
//# sourceMappingURL=author-desc-card.js.map