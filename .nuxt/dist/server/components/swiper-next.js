exports.ids = [13];
exports.modules = {

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".btn[data-v-5bcd429e]{transition:.4s;cursor:pointer}.box:hover .btn[data-v-5bcd429e]{color:#171717;transition:.4s;-webkit-backdrop-filter:blur(10px);backdrop-filter:blur(10px);background-color:transparent}.opacityImg[data-v-5bcd429e]{background:rgba(0,0,0,.8)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/SwiperNext.vue?vue&type=template&id=5bcd429e&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "grid lg:grid-cols-3 gap-30"
  }, [_vm._ssrNode("<div class=\"box lg:col-span-2 relative\" data-v-5bcd429e><p class=\"btn absolute hidden md:block top-15 right-15 px-30 py-15 border-2 font-semibold bg-black-primary text-yellow-primary border-yellow-secondary\" data-v-5bcd429e>Read more</p> <div class=\"w-full h-381 overflow-hidden\" data-v-5bcd429e>" + _vm._ssrList(_vm.images, function (img, index) {
    return index == _vm.count ? "<img" + _vm._ssrAttr("src", img.src) + " alt=\"img\" class=\"w-full h-full object-cover object-center\" data-v-5bcd429e>" : "<!---->";
  }) + "</div> <div class=\"p-30 bg-black-primary\" data-v-5bcd429e><h3 class=\"font-bold text-lg md:text-xl lg:text-xxl mb-15\" data-v-5bcd429e>Mirabad avenue</h3> <p class=\"line-clamp-2\" data-v-5bcd429e>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi, elementum rhoncus. Accumsan cursus eu </p></div></div> <div class=\"flex flex-col items-center justify-center\" data-v-5bcd429e><div class=\"w-full\" data-v-5bcd429e><div class=\"flex gap-15 mb-30\" data-v-5bcd429e><div class=\"p-10 border border-white-primary cursor-pointer\" data-v-5bcd429e><img" + _vm._ssrAttr("src", __webpack_require__(97)) + " width=\"27\" height=\"27\" alt=\"left\" data-v-5bcd429e></div> <div class=\"p-10 border border-white-primary cursor-pointer\" data-v-5bcd429e><img" + _vm._ssrAttr("src", __webpack_require__(98)) + " width=\"27\" height=\"27\" alt=\"right\" data-v-5bcd429e></div></div> <div class=\"w-full h-180 bg-white-primary overflow-hidden relative hidden lg:block\" data-v-5bcd429e><div class=\"opacityImg absolute top-0 left-0 w-full h-full\" data-v-5bcd429e></div> " + _vm._ssrList(_vm.images, function (img, index) {
    return index == _vm.active ? "<img" + _vm._ssrAttr("src", img.src) + " alt=\"img\" class=\"w-full h-full object-cover object-center\" data-v-5bcd429e>" : "<!---->";
  }) + "</div></div></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/SwiperNext.vue?vue&type=template&id=5bcd429e&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/SwiperNext.vue?vue&type=script&lang=js&
/* harmony default export */ var SwiperNextvue_type_script_lang_js_ = ({
  name: "MainSwiperNext",

  data() {
    return {
      count: 0,
      active: 1,
      images: [{
        id: 1,
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/1121098-pink-nature-wallpaper-1920x1080-lockscreen.jpg/1024px-1121098-pink-nature-wallpaper-1920x1080-lockscreen.jpg'
      }, {
        id: 2,
        src: 'https://images4.alphacoders.com/378/37864.jpg'
      }, {
        id: 3,
        src: 'https://wallpaper.dog/large/10950083.jpg'
      }, {
        id: 4,
        src: 'https://wallpaperfordesktop.com/wp-content/uploads/2021/11/Nature-Wallpapers.jpg'
      }, {
        id: 5,
        src: 'https://thumbs.dreamstime.com/b/green-leaf-texture-dew-drop-fresh-spring-nature-wallpaper-background-green-leaf-texture-dew-drop-fresh-nature-wallpaper-167543228.jpg'
      }],
      imgLength: 5
    };
  },

  methods: {
    AddActive() {
      if (this.count < this.imgLength - 1) {
        this.count += 1;
      } else {
        this.count = 0;
      }

      if (this.active < this.imgLength - 1) {
        this.active += 1;
      } else {
        this.active = 0;
      }
    },

    RemoveActive() {
      if (this.count > 0) {
        this.count -= 1;
      } else {
        this.count = this.imgLength - 1;
      }

      if (this.active > 0) {
        this.active -= 1;
      } else {
        this.active = this.imgLength - 1;
      }
    }

  }
});
// CONCATENATED MODULE: ./components/SwiperNext.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_SwiperNextvue_type_script_lang_js_ = (SwiperNextvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/SwiperNext.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(99)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_SwiperNextvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "5bcd429e",
  "7b94f51b"
  
)

/* harmony default export */ var SwiperNext = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(100);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("33a9beb0", content, true, context)
};

/***/ }),

/***/ 97:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAEUSURBVHgB7dvdSQQxFIbhM1ZgKbZgBdqBWIE1WcFqBWMHWoodxCwGxJ/1agbysc8D52J3Ll8SBjKpAgAAAAAAAAAAAAAAAAAgRGvtss/aPh2Ov4s5jViv7buHoi5qMmMlrX2ufjx6L+ZyYmW18Z8tcSZiBREriFhBxAoiVhCxgogVRKwgYgURK4hYQcQKIlYQsfaz1Mba6fOsc/PU535Zlk3P8fY4wLwpsY5u+9zVxvYIZsv7svkp+R7BHvu8FS99niuBl45AogUSLZBogUQLJFog0QKJFki0QKIFEi2QaIFECyRaINEC/RPNHeea8I7z+Abiun4fgrrjPLOx0g5jda22RAAAAAAAAAAAAAAAAAAA/vYBhv/fj0VlHVUAAAAASUVORK5CYII="

/***/ }),

/***/ 98:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAEQSURBVHgB7dvRacMwFIZRuRN0lI7QTtARMlsncDuBR0hH6QaqRAwh8atN9JNz4D74zfChi8F2KQAAAAAAAAAAAAAAAAAAsFFrfW2z1Iu5XxfG1QKd6q3zqNFeCt19nLc2i5M2qHUlnuvWsCft6YkWSLRAogUSLZBogUQLJFog0QKJFki0QKIFEi2QaIEeEW0qO1tvdG7zXp7bb5uPaZr+yo6OeIH5WcTq+kvQU9nZEcHs76tdT1d3RLCvNt+FvhJ/CuPwpBhErCBiBREriFhBxAoiVhCxgogVRKwgYgURK4hYQcQKIlaYuv3HedhY/nG+uA9zyPcY7GRdifN6shZrEAAAAAAAAAAAAAAAAAAgyT8vL92PT9hqxAAAAABJRU5ErkJggg=="

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(79);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ })

};;
//# sourceMappingURL=swiper-next.js.map