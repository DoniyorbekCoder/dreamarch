exports.ids = [3];
exports.modules = {

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AdvantageCard.vue?vue&type=template&id=47a7bb4c&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "box w-full py-30 px-15 flex flex-col justify-center items-center border border-white-secondary border-t-yellow-secondary relative"
  }, [_vm._ssrNode("<div class=\"w-54 h-54 shrink-0 mb-30 rounded-full border border-white-secondary flex items-center justify-center z-10\"><img" + _vm._ssrAttr("src", __webpack_require__(56)) + " width=\"24\" height=\"24\" alt=\"about\" class=\"object-cover object-center\"></div> <img" + _vm._ssrAttr("src", __webpack_require__(57)) + " alt=\"icon\" class=\"absolute top-60 right-15\"> <h3 class=\"font-bold text-lg md:text-xl leading-28 mb-5 z-10\">Advantage</h3> <p class=\"text-center text-base leading-24 line-clamp-4 z-10\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum</p>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AdvantageCard.vue?vue&type=template&id=47a7bb4c&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AdvantageCard.vue?vue&type=script&lang=js&
/* harmony default export */ var AdvantageCardvue_type_script_lang_js_ = ({
  name: "AdvantageCard"
});
// CONCATENATED MODULE: ./components/AdvantageCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AdvantageCardvue_type_script_lang_js_ = (AdvantageCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AdvantageCard.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AdvantageCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "4c182f7b"
  
)

/* harmony default export */ var AdvantageCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 56:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAANuSURBVHgB7d1NUhNBGAbgt4fIQjfcQLwBnkCqtLQsF+oJ1BPoFrSKUAW4FE+gngBdqVCWeAJzhHgDF5aFSNJ2J8RKMpP5Sfrnm+F9NlDJkCq+d+ab0N1pACIiogY63MONox3ch2AKDXW0iy2t0Lbfa6Db6mP95gv8gDAJGmi8+JY5y1Z7Cp1Pu3gKYRp3BUwXf5q5GvZ/n2L7YRs/IUCjroCi4lvmjHt2ZRkHEKIxAZQp/ph1Ke2oEQFULP6A+cUrHe9L7QOYp/gDCitfdnAVkdU6gLmLf+50CdcQWW0DWLT41skfdBBZLQNwUXyjI+GtaO0CcFR8+wdB9LPfqlUAzopvqCW8hwC1CcBl8a3kjFdAaa6Lb3SkDMyJD8BD8cX0f0t0AF6KDzn93xIbgK/iW79O8A1CiAzAZ/Eh5P3/iLgAPBffzgccQxBRAfguvpUkDCBTiOJbkvq/JSKAUMWHsP5vRQ8gYPHF9X8ragAhi2/1FT5AmGgBhC6+JWH8f1qUAGIU30xBHkvr/1YLgX3ewyMdYUJcCxr/GRd8YdbhHr6bL2tosmHYnSWNdtGoa4wANC6QopV4jVwbKslgJd4lfD1oYyXreQYQgsLa5WVsZT+V4eNLrJr+9QbDXr2CCm5v5re1i9aCpqyb+kwMhaTeBZ0X394oKxWeipkz74H5MhFAqgW1NF6BxfdCDQOYkApAZxxEzqxOP8CbcGQMIDIGEBkDiIwBRMYAImMAkTGAyBhAZAwgJJ2eE2AAISXpaVEGEFCi8Tb1GCiU7q1NvJt+kAGEorNX5TGAQOwKiazHGUAICsezlqcwgACybr7/nwP5lnnzHWEAnmk1++y3GIBnrR4DiMf0/qK1oQzAo14yu/ePMAB/unc3ij8SxQA8KbspIAPwRPXLfSCQAfhQ4uY7wgA8+Au8LnssA3Cvc+95+c+jMQDHTEH3Kx5PDuWO+2RhAC7p6lshMACHZk265GEAruRMuuRhAI7kTbrk/hzIhco33xEG4EDRpEseBuBA0aRLHgawqArjPlkYwILKTLrkYQCLKTXpkicrAN+7SonbtWpeiYONp1IBKPjdWcr364dUdtIlTyqAM4Un8HiW+n79UOyedy7+B0EqANPTuj2F6yrxs8W779f3TZkhB1O0x3c2sA0iIiIiIiIiIqIK/gEPTAxlIXk0dwAAAABJRU5ErkJggg=="

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAANjSURBVHgB7d3bcdpAFAbgw3W46AEqiNMBqSB2BaGDJBWkBOMK4lQQ3EHSgVJB6CBOBfiByww3+awHPViIRYK9HMn/N+NhRgjZc37trtgFmQgAAKCElsvlx/l8/onAPS787Ww2i/Y//ziMdwRuJIof/0wXi8U3EqZCJaOKH0XRSLPL/Xq9vuv3+08kQKkCyFD8WBgEwQ0JUKWSyFF85VpKd1SKFpCz+LEnbgV98qzwLeDM4is9fp33K6NCB3BB8V/wldF78qywAVxafGWz2UzIs0IGYKL4bCLhUrRwARgqPlUqFe9nv1KoAEwVf+8XCVCYAAwXnzqdDlpAVqaLzybcBf0nAcQHYKH4Yvp/RXQANoq/J6L/V8QGYLH4tFqt/pAQIgOwWXwScv0fExeA5eIrIQkiKgAHxVcDcEiCiAnARfEVSf2/IiIAV8UnYf2/4j0Ah8VXQhLGawCOi0/8u36TMN4CcF18RcL8f5KXAHwUn4XS+n/FeQC8DPjZQ/EVcWe/4vxTERzAX34YULmpsCe1Wm3Ubre1s64+AojobdF+Eg8BuDHhEG7SQijNJ+OEGzQajdu0J1JbwHQ6veIX/OTBcsBzJz3KIQgCbat6oy3gBY8J1zwmvJoKqSd3UsWv1+tqoOxx8QnM2W63Q354FcBBF9RsNr/nPeshs2Fyw0EA3O0MCWy5Sm7AIOwZAvAMAXiGADxDAJ4hAM8QgGcIwDME4BkCcAvT0Z4dLIsiAId4Onqc3IYAHOEZ5kdeC3hIbkcA7oRpGxGAI51OZ5S2HQG4ER77UiACcCBt8I0hAMuODb4xBGDfWPckArCMB9+x7nkEYBF3P+NT38hHABbtdruHU/vUCaxQg28QBOGp/dACLKlWq6NM+xFY0Wq1wiz7IQALsgy+MQRgQRRFP7LuiwDMm/Dgm/n7aAjAMJ73uc+zPwIw6NS8TxoEYFZIOSEAg44tuuggAHPCc+7EiAAM0S266CAAA84ZfGMIwIwxnQkBGHBq0UUHAVwoz7xPGgRwoSyLLjpYkLlA1kUXnbQWYPuuUuLuWnWurIsu2mOkbLN9ZymRd646R9ZFF52DAPgNxVeyeJbaPr4r3P2MTPwPgoMA+A3FIxfpAx/cyi3ebR/fgZD//i/dbveOAAAAAAAAAAAAcngGdvB8ZzaiUasAAAAASUVORK5CYII="

/***/ })

};;
//# sourceMappingURL=advantage-card.js.map