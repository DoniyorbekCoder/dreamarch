exports.ids = [12];
exports.modules = {

/***/ 53:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("312dc31a", content, true, context)
};

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/StatisticCard.vue?vue&type=template&id=6ee27786&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    class: `StaticCard shrink-0 w-${_vm.width}  h-${_vm.width} border-4 rounded-full border-transparent ${_vm.bgColor}`
  }, [_vm._ssrNode("<div" + _vm._ssrClass(null, `w-full h-full rounded-full flex flex-col items-center px-45 justify-center shrink-0 ${_vm.color}`) + " data-v-6ee27786><h3 class=\"font-bold text-vll font-mak\" data-v-6ee27786>" + _vm._ssrEscape(_vm._s(_vm.width)) + "</h3> <p class=\"line-clamp-4 text-base max-w-202 text-center\" data-v-6ee27786>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum</p></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/StatisticCard.vue?vue&type=template&id=6ee27786&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/StatisticCard.vue?vue&type=script&lang=js&
/* harmony default export */ var StatisticCardvue_type_script_lang_js_ = ({
  name: "StatisticCard",
  props: {
    color: {
      required: true,
      type: String
    },
    bgColor: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      width: '322'
    };
  },

  methods: {
    CardWidth() {
      const element = document.querySelector('.StaticCard');
      this.width = element.scrollWidth; // offsetWidth  offsetHeight clientWidth

      const elements = document.querySelectorAll('.StaticCard');
      elements.forEach(card => {
        card.style.height = `${this.width + 8}px`;
        card.style.width = `${this.width + 8}px`;
      });
      console.log(this.width);
      setTimeout(() => {
        this.CardWidth();
      }, 100);
    }

  },

  mounted() {
    this.CardWidth();
  }

});
// CONCATENATED MODULE: ./components/StatisticCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_StatisticCardvue_type_script_lang_js_ = (StatisticCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/StatisticCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(71)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_StatisticCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6ee27786",
  "db3f28c0"
  
)

/* harmony default export */ var StatisticCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(53);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".statistics__item[data-v-6ee27786]{background-size:100% 100%,50% 50%,50% 50%,50% 50%,50% 50%;background-repeat:no-repeat;background-position:50%,0 0,100% 0,0 100%,100% 100%;background-origin:content-box,border-box,border-box,border-box,border-box;background-clip:content-box,border-box,border-box,border-box,border-box}.statistics__item[data-v-6ee27786]:nth-child(3n-2){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#282828 36%,#282828 0),linear-gradient(10deg,#ffda79,#ffda79 0),linear-gradient(300deg,#282828 36%,#282828 0),linear-gradient(210deg,#ffda79 60%,#282828 0)}.statistics__item[data-v-6ee27786]:nth-child(3n-1){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#282828 36%,#282828 0),linear-gradient(10deg,#ffda79,#282828 0),linear-gradient(300deg,#ffda79 66%,#ffda79 0),linear-gradient(210deg,#282828 60%,#ffda79 0)}.statistics__item[data-v-6ee27786]:nth-child(3n){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#ffda79 36%,#ffda79 0),linear-gradient(10deg,#ffda79,#282828 0),linear-gradient(300deg,#282828 36%,#ffda79 0),linear-gradient(210deg,#282828 60%,#282828 0)}.statistics__item2[data-v-6ee27786]{background-size:100% 100%,50% 50%,50% 50%,50% 50%,50% 50%;background-repeat:no-repeat;background-position:50%,0 0,100% 0,0 100%,100% 100%;background-origin:content-box,border-box,border-box,border-box,border-box;background-clip:content-box,border-box,border-box,border-box,border-box}.statistics__item2[data-v-6ee27786]:nth-child(3n-2){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(10deg,#ffda79,#ffda79 0),linear-gradient(300deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(210deg,#ffda79 60%,#f4f4f4 0)}.statistics__item2[data-v-6ee27786]:nth-child(3n-1){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(10deg,#ffda79,#f4f4f4 0),linear-gradient(300deg,#ffda79 66%,#ffda79 0),linear-gradient(210deg,#f4f4f4 60%,#ffda79 0)}.statistics__item2[data-v-6ee27786]:nth-child(3n){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#ffda79 36%,#ffda79 0),linear-gradient(10deg,#ffda79,#f4f4f4 0),linear-gradient(300deg,#f4f4f4 36%,#ffda79 0),linear-gradient(210deg,#f4f4f4 60%,#f4f4f4 0)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=statistic-card.js.map