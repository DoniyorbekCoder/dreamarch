exports.ids = [1];
exports.modules = {

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AboutAccordion.vue?vue&type=template&id=310676de&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('vsa-list', {
    staticClass: "list"
  }, [_c('vsa-item', [_c('vsa-heading', [_vm._v("How Much Does it Cost to Make an App?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1), _vm._v(" "), _c('vsa-item', [_c('vsa-heading', [_vm._v("What are the main steps to building an app?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1), _vm._v(" "), _c('vsa-item', [_c('vsa-heading', [_vm._v("What is the main difference between a website and a web\n        app?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1), _vm._v(" "), _c('vsa-item', [_c('vsa-heading', [_vm._v("What Is Mobile App Development Services?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1)], 1);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AboutAccordion.vue?vue&type=template&id=310676de&

// EXTERNAL MODULE: external "vue-simple-accordion"
var external_vue_simple_accordion_ = __webpack_require__(41);

// EXTERNAL MODULE: ./node_modules/vue-simple-accordion/dist/vue-simple-accordion.css
var vue_simple_accordion = __webpack_require__(90);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AboutAccordion.vue?vue&type=script&lang=js&


/* harmony default export */ var AboutAccordionvue_type_script_lang_js_ = ({
  name: 'AboutAccordionCard',
  components: {
    VsaList: external_vue_simple_accordion_["VsaList"],
    VsaItem: external_vue_simple_accordion_["VsaItem"],
    VsaHeading: external_vue_simple_accordion_["VsaHeading"],
    VsaContent: external_vue_simple_accordion_["VsaContent"],
    VsaIcon: external_vue_simple_accordion_["VsaIcon"]
  }
});
// CONCATENATED MODULE: ./components/AboutAccordion.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AboutAccordionvue_type_script_lang_js_ = (AboutAccordionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AboutAccordion.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(92)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AboutAccordionvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "27cee5cb"
  
)

/* harmony default export */ var AboutAccordion = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(93);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("207f51a7", content, true, context)
};

/***/ }),

/***/ 90:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(91);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("51b2c2d9", content, true)

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".vsa-item__heading{width:100%;height:100%}.vsa-item__heading,.vsa-item__trigger{display:flex;justify-content:flex-start;align-items:center}.vsa-item__trigger{margin:0;padding:0;color:inherit;font-family:inherit;font-size:100%;line-height:1.15;border-width:0;background-color:transparent;background-image:none;overflow:visible;text-transform:none;flex:1 1 auto;color:var(--vsa-text-color);transition:all .2s linear;padding:var(--vsa-heading-padding)}.vsa-item__trigger[role=button]{cursor:pointer}.vsa-item__trigger[type=button],.vsa-item__trigger[type=reset],.vsa-item__trigger[type=submit]{-webkit-appearance:button}.vsa-item__trigger:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}.vsa-item__trigger::-moz-focus-inner,.vsa-item__trigger[type=button]::-moz-focus-inner,.vsa-item__trigger[type=reset]::-moz-focus-inner,.vsa-item__trigger[type=submit]::-moz-focus-inner{border-style:none;padding:0}.vsa-item__trigger:-moz-focusring,.vsa-item__trigger[type=button]:-moz-focusring,.vsa-item__trigger[type=reset]:-moz-focusring,.vsa-item__trigger[type=submit]:-moz-focusring{outline:1px dotted ButtonText}.vsa-item__trigger:focus,.vsa-item__trigger:hover{outline:none;background-color:var(--vsa-highlight-color);color:var(--vsa-bg-color)}.vsa-item__trigger__icon--is-default{width:40px;height:40px;transform:scale(var(--vsa-default-icon-size))}.vsa-item__trigger__icon--is-default:after,.vsa-item__trigger__icon--is-default:before{background-color:var(--vsa-text-color);content:\"\";height:3px;position:absolute;top:10px;transition:all .13333s ease-in-out;width:30px}.vsa-item__trigger__icon--is-default:before{left:0;transform:rotate(45deg) translate3d(8px,22px,0);transform-origin:100%}.vsa-item__trigger__icon--is-default:after{transform:rotate(-45deg) translate3d(-8px,22px,0);right:0;transform-origin:0}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:before{transform:rotate(45deg) translate3d(14px,14px,0)}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:after{transform:rotate(-45deg) translate3d(-14px,14px,0)}.vsa-item__trigger__icon{display:block;margin-left:auto;position:relative;transition:all .2s ease-in-out}.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:before,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:before{background-color:var(--vsa-bg-color)}.vsa-item__trigger__content{font-weight:700;font-size:1.25rem}.vsa-item__content{margin:0;padding:var(--vsa-content-padding)}.vsa-item--is-active .vsa-item__heading,.vsa-item:not(:last-of-type){border-bottom:var(--vsa-border)}.vsa-collapse-enter-active,.vsa-collapse-leave-active{transition-property:opacity,height,padding-top,padding-bottom;transition-duration:.3s;transition-timing-function:ease-in-out}.vsa-collapse-enter,.vsa-collapse-leave-active{opacity:0;height:0;padding-top:0;padding-bottom:0;overflow:hidden}.vsa-list{--vsa-max-width:720px;--vsa-min-width:300px;--vsa-heading-padding:1rem 1rem;--vsa-text-color:#373737;--vsa-highlight-color:#57a;--vsa-bg-color:#fff;--vsa-border-color:rgba(0,0,0,0.2);--vsa-border-width:1px;--vsa-border-style:solid;--vsa-border:var(--vsa-border-width) var(--vsa-border-style) var(--vsa-border-color);--vsa-content-padding:1rem 1rem;--vsa-default-icon-size:1;display:block;max-width:var(--vsa-max-width);min-width:var(--vsa-min-width);width:100%;padding:0;margin:0;list-style:none;border:var(--vsa-border);color:var(--vsa-text-color);background-color:var(--vsa-bg-color)}.vsa-list [hidden]{display:none}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".list{max-width:100%;transition:all 2s ease-in-out}.vsa-item--is-active .vsa-item__heading,.vsa-item:not(:last-of-type){border-bottom:none;transition:all 2s ease-in-out}.vsa-item__content{opacity:1;padding-top:0;font-style:normal;font-weight:400;font-size:16px;line-height:24px;color:#171717;background-color:#f4f4f4}.vsa-item__trigger__content{text-align:start;font-weight:600;font-size:32px;color:#171717}.vsa-item__trigger:focus,.vsa-item__trigger:hover{outline:none!important;background-color:#f4f4f4;color:#000!important}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:before{left:32px!important}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:after{left:27px!important}.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:before,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:before{background-color:#000!important}.vsa-item__trigger__icon--is-default:before{left:31px!important}.vsa-item__trigger{align-items:flex-start;padding:25px 30px!important}.vsa-item__trigger__icon--is-default{min-width:40px!important}.vsa-item__trigger__icon--is-default:after,.vsa-item__trigger__icon--is-default:before{width:15px}@media screen and (max-width:1024px){.vsa-item__trigger{align-items:flex-start;padding:20px 25px!important}.vsa-item__trigger__content{font-size:26px}.vsa-item__content{padding:0 25px}}@media screen and (max-width:768px){.vsa-item__trigger{align-items:flex-start;padding:15px 20px!important}.vsa-item__trigger__content{font-size:20px}.vsa-item__content{padding:0 20px}}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=about-accordion.js.map