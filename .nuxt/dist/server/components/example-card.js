exports.ids = [7];
exports.modules = {

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(60);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("8f8becac", content, true, context)
};

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ExampleCard.vue?vue&type=template&id=9835ec8c&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "box w-full md:h-335 border border-white-secondary relative hover:cursor-pointer"
  }, [_vm._ssrNode("<p" + _vm._ssrClass("btn absolute hidden md:block top-15 right-15 px-30 py-15 border-2 font-semibold", [_vm.index == '1' ? 'bg-yellow-primary text-black-primary border-yellow-secondary' : 'bg-black-primary text-yellow-primary border-yellow-secondary']) + " data-v-9835ec8c>Read more</p> <img" + _vm._ssrAttr("src", __webpack_require__(58)) + " alt=\"house\" class=\"w-full h-154 md:h-full object-cover object-center\" data-v-9835ec8c> <div class=\"bg block md:hidden p-15 text-white-primary\" data-v-9835ec8c><h3 class=\"font-bold text-lg leading-24 md:text-lg mb-5\" data-v-9835ec8c>Mirabad avenue</h3> <p class=\"text-sm leading-21\" data-v-9835ec8c>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi</p></div> " + (_vm.index === '1' ? "<div class=\"absolute hidden md:block bottom-0 w-full p-15 md:p-30 bg text-white-primary\" data-v-9835ec8c><h3 class=\"font-bold text-xl leading-29 mb-5\" data-v-9835ec8c>Mirabad avenue</h3> <p class=\"text-base leading-24 line-clamp-2\" data-v-9835ec8c>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi, elementum rhoncus. Acc</p></div>" : "<!---->"))]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ExampleCard.vue?vue&type=template&id=9835ec8c&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ExampleCard.vue?vue&type=script&lang=js&
/* harmony default export */ var ExampleCardvue_type_script_lang_js_ = ({
  name: "ExampleCard",
  props: {
    index: {
      // required: true,
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/ExampleCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ExampleCardvue_type_script_lang_js_ = (ExampleCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ExampleCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(59)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ExampleCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "9835ec8c",
  "565a688c"
  
)

/* harmony default export */ var ExampleCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/house2.3b4168e.png";

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(48);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".btn[data-v-9835ec8c]{transition:.4s;cursor:pointer}.box:hover .btn[data-v-9835ec8c]{color:#171717;transition:.4s;background-color:transparent}.bg[data-v-9835ec8c],.box:hover .btn[data-v-9835ec8c]{-webkit-backdrop-filter:blur(10px);backdrop-filter:blur(10px)}.bg[data-v-9835ec8c]{background:hsla(0,0%,9%,.5)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=example-card.js.map