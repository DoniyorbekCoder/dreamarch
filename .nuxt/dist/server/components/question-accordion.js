exports.ids = [9];
exports.modules = {

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/QuestionAccordion.vue?vue&type=template&id=5bc7ee2a&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "accordion accordion-flushborder border-gray-primary rounded-2px",
    attrs: {
      "id": "accordionFlushExample"
    }
  }, [_vm._ssrNode("<div class=\"accordion-item\" data-v-5bc7ee2a><h2 id=\"flush-headingOne\" class=\"accordion-header font-semibold text-lg md:text-xl lg:text-xxl\" data-v-5bc7ee2a><button type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseOne\" aria-expanded=\"false\" aria-controls=\"flush-collapseOne\" class=\"accordion-button font-semibold text-lg md:text-xl lg:text-xxl collapsed\" data-v-5bc7ee2a>\n                Accordion Item #1\n            </button></h2> <div id=\"flush-collapseOne\" aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\" class=\"accordion-collapse collapse\" data-v-5bc7ee2a><div class=\"accordion-body pt-0\" data-v-5bc7ee2a>Placeholder content for this accordion, which is intended to demonstrate the <code data-v-5bc7ee2a>.accordion-flush</code> class. This is the first item's accordion body.</div></div></div> <div class=\"accordion-item\" data-v-5bc7ee2a><h2 id=\"flush-headingTwo\" class=\"accordion-header font-semibold text-lg md:text-xl lg:text-xxl\" data-v-5bc7ee2a><button type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseTwo\" aria-expanded=\"false\" aria-controls=\"flush-collapseTwo\" class=\"accordion-button font-semibold text-lg md:text-xl lg:text-xxl collapsed\" data-v-5bc7ee2a>\n                Accordion Item #2\n            </button></h2> <div id=\"flush-collapseTwo\" aria-labelledby=\"flush-headingTwo\" data-bs-parent=\"#accordionFlushExample\" class=\"accordion-collapse collapse\" data-v-5bc7ee2a><div class=\"accordion-body pt-0\" data-v-5bc7ee2a>Placeholder content for this accordion, which is intended to demonstrate the <code data-v-5bc7ee2a>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div></div></div> <div class=\"accordion-item\" data-v-5bc7ee2a><h2 id=\"flush-headingThree\" class=\"accordion-header font-semibold text-lg md:text-xl lg:text-xxl\" data-v-5bc7ee2a><button type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseThree\" aria-expanded=\"false\" aria-controls=\"flush-collapseThree\" class=\"accordion-button font-semibold text-lg md:text-xl lg:text-xxl collapsed\" data-v-5bc7ee2a>\n                Accordion Item #3\n            </button></h2> <div id=\"flush-collapseThree\" aria-labelledby=\"flush-headingThree\" data-bs-parent=\"#accordionFlushExample\" class=\"accordion-collapse collapse\" data-v-5bc7ee2a><div class=\"accordion-body pt-0\" data-v-5bc7ee2a>Placeholder content for this accordion, which is intended to demonstrate the <code data-v-5bc7ee2a>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div></div></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/QuestionAccordion.vue?vue&type=template&id=5bc7ee2a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/QuestionAccordion.vue?vue&type=script&lang=js&
/* harmony default export */ var QuestionAccordionvue_type_script_lang_js_ = ({
  name: "QuestionAccordion",
  head: {
    link: [{
      rel: "stylesheet",
      href: "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css",
      integrity: "sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC",
      crossorigin: "anonymous"
    }],
    script: [{
      src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js'
    }]
  }
});
// CONCATENATED MODULE: ./components/QuestionAccordion.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_QuestionAccordionvue_type_script_lang_js_ = (QuestionAccordionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/QuestionAccordion.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(87)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_QuestionAccordionvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "5bc7ee2a",
  "7e627bbc"
  
)

/* harmony default export */ var QuestionAccordion = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(88);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("4d3ccba0", content, true, context)
};

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(73);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(20);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(89);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".accordion-body[data-v-5bc7ee2a]{padding-top:0}.accordion-button[data-v-5bc7ee2a]:not(.collapsed):after{background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");transform:rotate(-180deg)}.accordion-button[data-v-5bc7ee2a]:after{flex-shrink:0;width:2rem;height:2rem;margin-left:auto;content:\"\";background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");background-repeat:no-repeat;background-size:2rem;transition:transform .2s ease-in-out}.accordion-button[data-v-5bc7ee2a]:focus{z-index:3;outline:none;border:none;box-shadow:none}.accordion-button[data-v-5bc7ee2a]:not(.collapsed){color:#171717;background-color:transparent;box-shadow:none}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 89:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAI2SURBVHgB7dytbpNRAMbxUzAkmwCxzSKRSC4HJA4cDosbdwC3wCVwO5sAswQ3zpMgYKFZ99Xz9fslzZpWnT7/7K15WwoAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6zk+Pv54cnLyI4+jo6PTsphezv+4NFAPfbrZbD7Up0/yqM9fHRwcPL+4uPhWFlDH/1LP/K78e/5Sz/+97NmmNJDq65+nV1+/vLz8enZ29qZM7M/4r6++Xs/+s579WdmzR6Uj+WDyAZVJbRu/pSYB1No/b3tv1giuGz///UoDTb4D5FqXa379QF7+7/28PtN3gl3GPz8/f1saaBJAZNwVIthl/Jbfe5oFELNH0Pv40TSAmDWCEcaP5gHEbBGMMn50EUDMEsFI40c3AcToEYw2fnQVQIwawYjjR3cBxGgRjDp+dBlAjBLByONHtwFE7xGMPn50HUD0GsEM40f3AURvEcwyfgwRQPQSwUzjxzABROsIZhs/hgogWkUw4/gxXACx7whmHT+GDCD2FcHM48ewAcRDRzD7+DF0APFQEawwfgwfQNx3BKuMH1MEEPcVwUrjxzQBxF0jWG38mCqAuG0EK44f0wUQN41g1fGjyb2B+7Lr3Tirjh9TBxB3uR9vhZtVp7wE/O26y8E2K4wf0wcQN41glfFjiQBi1whWGj+WCSCui2C18WOpAGJbBCuOH8sFEIng8PAwP1Hzoj5+1fE/1fHfFwAAAAAAAAAAAAAAAAAAAAAAAAAAAADYp99ySRW0Oml9MQAAAABJRU5ErkJggg=="

/***/ })

};;
//# sourceMappingURL=question-accordion.js.map