exports.ids = [2,3,7];
exports.modules = {

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(60);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("8f8becac", content, true, context)
};

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AdvantageCard.vue?vue&type=template&id=47a7bb4c&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "box w-full py-30 px-15 flex flex-col justify-center items-center border border-white-secondary border-t-yellow-secondary relative"
  }, [_vm._ssrNode("<div class=\"w-54 h-54 shrink-0 mb-30 rounded-full border border-white-secondary flex items-center justify-center z-10\"><img" + _vm._ssrAttr("src", __webpack_require__(56)) + " width=\"24\" height=\"24\" alt=\"about\" class=\"object-cover object-center\"></div> <img" + _vm._ssrAttr("src", __webpack_require__(57)) + " alt=\"icon\" class=\"absolute top-60 right-15\"> <h3 class=\"font-bold text-lg md:text-xl leading-28 mb-5 z-10\">Advantage</h3> <p class=\"text-center text-base leading-24 line-clamp-4 z-10\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum</p>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AdvantageCard.vue?vue&type=template&id=47a7bb4c&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AdvantageCard.vue?vue&type=script&lang=js&
/* harmony default export */ var AdvantageCardvue_type_script_lang_js_ = ({
  name: "AdvantageCard"
});
// CONCATENATED MODULE: ./components/AdvantageCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AdvantageCardvue_type_script_lang_js_ = (AdvantageCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AdvantageCard.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AdvantageCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "4c182f7b"
  
)

/* harmony default export */ var AdvantageCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ExampleCard.vue?vue&type=template&id=9835ec8c&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "box w-full md:h-335 border border-white-secondary relative hover:cursor-pointer"
  }, [_vm._ssrNode("<p" + _vm._ssrClass("btn absolute hidden md:block top-15 right-15 px-30 py-15 border-2 font-semibold", [_vm.index == '1' ? 'bg-yellow-primary text-black-primary border-yellow-secondary' : 'bg-black-primary text-yellow-primary border-yellow-secondary']) + " data-v-9835ec8c>Read more</p> <img" + _vm._ssrAttr("src", __webpack_require__(58)) + " alt=\"house\" class=\"w-full h-154 md:h-full object-cover object-center\" data-v-9835ec8c> <div class=\"bg block md:hidden p-15 text-white-primary\" data-v-9835ec8c><h3 class=\"font-bold text-lg leading-24 md:text-lg mb-5\" data-v-9835ec8c>Mirabad avenue</h3> <p class=\"text-sm leading-21\" data-v-9835ec8c>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi</p></div> " + (_vm.index === '1' ? "<div class=\"absolute hidden md:block bottom-0 w-full p-15 md:p-30 bg text-white-primary\" data-v-9835ec8c><h3 class=\"font-bold text-xl leading-29 mb-5\" data-v-9835ec8c>Mirabad avenue</h3> <p class=\"text-base leading-24 line-clamp-2\" data-v-9835ec8c>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi, elementum rhoncus. Acc</p></div>" : "<!---->"))]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ExampleCard.vue?vue&type=template&id=9835ec8c&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ExampleCard.vue?vue&type=script&lang=js&
/* harmony default export */ var ExampleCardvue_type_script_lang_js_ = ({
  name: "ExampleCard",
  props: {
    index: {
      // required: true,
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/ExampleCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ExampleCardvue_type_script_lang_js_ = (ExampleCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ExampleCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(59)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ExampleCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "9835ec8c",
  "565a688c"
  
)

/* harmony default export */ var ExampleCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(77);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("9da37c62", content, true, context)
};

/***/ }),

/***/ 55:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/i.8ed363a.png";

/***/ }),

/***/ 56:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAANuSURBVHgB7d1NUhNBGAbgt4fIQjfcQLwBnkCqtLQsF+oJ1BPoFrSKUAW4FE+gngBdqVCWeAJzhHgDF5aFSNJ2J8RKMpP5Sfrnm+F9NlDJkCq+d+ab0N1pACIiogY63MONox3ch2AKDXW0iy2t0Lbfa6Db6mP95gv8gDAJGmi8+JY5y1Z7Cp1Pu3gKYRp3BUwXf5q5GvZ/n2L7YRs/IUCjroCi4lvmjHt2ZRkHEKIxAZQp/ph1Ke2oEQFULP6A+cUrHe9L7QOYp/gDCitfdnAVkdU6gLmLf+50CdcQWW0DWLT41skfdBBZLQNwUXyjI+GtaO0CcFR8+wdB9LPfqlUAzopvqCW8hwC1CcBl8a3kjFdAaa6Lb3SkDMyJD8BD8cX0f0t0AF6KDzn93xIbgK/iW79O8A1CiAzAZ/Eh5P3/iLgAPBffzgccQxBRAfguvpUkDCBTiOJbkvq/JSKAUMWHsP5vRQ8gYPHF9X8ragAhi2/1FT5AmGgBhC6+JWH8f1qUAGIU30xBHkvr/1YLgX3ewyMdYUJcCxr/GRd8YdbhHr6bL2tosmHYnSWNdtGoa4wANC6QopV4jVwbKslgJd4lfD1oYyXreQYQgsLa5WVsZT+V4eNLrJr+9QbDXr2CCm5v5re1i9aCpqyb+kwMhaTeBZ0X394oKxWeipkz74H5MhFAqgW1NF6BxfdCDQOYkApAZxxEzqxOP8CbcGQMIDIGEBkDiIwBRMYAImMAkTGAyBhAZAwgJJ2eE2AAISXpaVEGEFCi8Tb1GCiU7q1NvJt+kAGEorNX5TGAQOwKiazHGUAICsezlqcwgACybr7/nwP5lnnzHWEAnmk1++y3GIBnrR4DiMf0/qK1oQzAo14yu/ePMAB/unc3ij8SxQA8KbspIAPwRPXLfSCQAfhQ4uY7wgA8+Au8LnssA3Cvc+95+c+jMQDHTEH3Kx5PDuWO+2RhAC7p6lshMACHZk265GEAruRMuuRhAI7kTbrk/hzIhco33xEG4EDRpEseBuBA0aRLHgawqArjPlkYwILKTLrkYQCLKTXpkicrAN+7SonbtWpeiYONp1IBKPjdWcr364dUdtIlTyqAM4Un8HiW+n79UOyedy7+B0EqANPTuj2F6yrxs8W779f3TZkhB1O0x3c2sA0iIiIiIiIiIqIK/gEPTAxlIXk0dwAAAABJRU5ErkJggg=="

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAANjSURBVHgB7d3bcdpAFAbgw3W46AEqiNMBqSB2BaGDJBWkBOMK4lQQ3EHSgVJB6CBOBfiByww3+awHPViIRYK9HMn/N+NhRgjZc37trtgFmQgAAKCElsvlx/l8/onAPS787Ww2i/Y//ziMdwRuJIof/0wXi8U3EqZCJaOKH0XRSLPL/Xq9vuv3+08kQKkCyFD8WBgEwQ0JUKWSyFF85VpKd1SKFpCz+LEnbgV98qzwLeDM4is9fp33K6NCB3BB8V/wldF78qywAVxafGWz2UzIs0IGYKL4bCLhUrRwARgqPlUqFe9nv1KoAEwVf+8XCVCYAAwXnzqdDlpAVqaLzybcBf0nAcQHYKH4Yvp/RXQANoq/J6L/V8QGYLH4tFqt/pAQIgOwWXwScv0fExeA5eIrIQkiKgAHxVcDcEiCiAnARfEVSf2/IiIAV8UnYf2/4j0Ah8VXQhLGawCOi0/8u36TMN4CcF18RcL8f5KXAHwUn4XS+n/FeQC8DPjZQ/EVcWe/4vxTERzAX34YULmpsCe1Wm3Ubre1s64+AojobdF+Eg8BuDHhEG7SQijNJ+OEGzQajdu0J1JbwHQ6veIX/OTBcsBzJz3KIQgCbat6oy3gBY8J1zwmvJoKqSd3UsWv1+tqoOxx8QnM2W63Q354FcBBF9RsNr/nPeshs2Fyw0EA3O0MCWy5Sm7AIOwZAvAMAXiGADxDAJ4hAM8QgGcIwDME4BkCcAvT0Z4dLIsiAId4Onqc3IYAHOEZ5kdeC3hIbkcA7oRpGxGAI51OZ5S2HQG4ER77UiACcCBt8I0hAMuODb4xBGDfWPckArCMB9+x7nkEYBF3P+NT38hHABbtdruHU/vUCaxQg28QBOGp/dACLKlWq6NM+xFY0Wq1wiz7IQALsgy+MQRgQRRFP7LuiwDMm/Dgm/n7aAjAMJ73uc+zPwIw6NS8TxoEYFZIOSEAg44tuuggAHPCc+7EiAAM0S266CAAA84ZfGMIwIwxnQkBGHBq0UUHAVwoz7xPGgRwoSyLLjpYkLlA1kUXnbQWYPuuUuLuWnWurIsu2mOkbLN9ZymRd646R9ZFF52DAPgNxVeyeJbaPr4r3P2MTPwPgoMA+A3FIxfpAx/cyi3ebR/fgZD//i/dbveOAAAAAAAAAAAAcngGdvB8ZzaiUasAAAAASUVORK5CYII="

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/house2.3b4168e.png";

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(48);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleCard_vue_vue_type_style_index_0_id_9835ec8c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".btn[data-v-9835ec8c]{transition:.4s;cursor:pointer}.box:hover .btn[data-v-9835ec8c]{color:#171717;transition:.4s;background-color:transparent}.bg[data-v-9835ec8c],.box:hover .btn[data-v-9835ec8c]{-webkit-backdrop-filter:blur(10px);backdrop-filter:blur(10px)}.bg[data-v-9835ec8c]{background:hsla(0,0%,9%,.5)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AboutContainer.vue?vue&type=template&id=60cbfc4a&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "container relative"
  }, [_vm._ssrNode("<div class=\"bg-white-primary p-30 md:p-48 lg:p-60 pb-30 flex flex-col gap-60 relative\" data-v-60cbfc4a>", "</div>", [_vm._ssrNode("<p class=\"absoluteText absolute top-90 leading-60 md:leading-180 left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-primary\" data-v-60cbfc4a>" + _vm._ssrEscape(_vm._s(_vm.name)) + "</p> <h2 class=\"font-medium text-Title md:text-mdTitle lg:text-lgTitle uppercase leading-39 md:leading-62 z-10\" data-v-60cbfc4a>a brief illustrated title on Architecture</h2> <div class=\"flex flex-col md:flex-row gap-15 z-10\" data-v-60cbfc4a><div class=\"w-54 h-54 shrink-0 rounded-full border border-white-secondary flex items-center justify-center\" data-v-60cbfc4a><img" + _vm._ssrAttr("src", __webpack_require__(55)) + " width=\"24\" height=\"24\" alt=\"about\" class=\"object-cover object-center\" data-v-60cbfc4a></div> <p class=\"text-base leading-24\" data-v-60cbfc4a>Architecture is the art and science of enhancing the interior of a building to achieve a healthier and more aesthetically pleasing environment for the people using the space. An interior designer is someone who plans, researches, coordinates, and manages such enhancement projects. Interior design is a multifaceted profession that includes conceptual development, space planning, site inspections, programming, research, communicating with the stakeholders of a project, construction management, and execution of the design.</p></div> "), _vm._ssrNode("<div class=\"grid md:grid-cols-2 lg:grid-cols-3 gap-15\" data-v-60cbfc4a>", "</div>", [_c('AdvantageCard'), _vm._ssrNode(" "), _c('AdvantageCard'), _vm._ssrNode(" "), _c('AdvantageCard')], 2), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"styleGrid grid lg:lg:grid-cols-3 gap-15\" data-v-60cbfc4a>", "</div>", [_c('ExampleCard', {
    attrs: {
      "index": "1"
    }
  }), _vm._ssrNode(" "), _c('ExampleCard', {
    attrs: {
      "index": "2"
    }
  }), _vm._ssrNode(" "), _c('ExampleCard', {
    attrs: {
      "index": "2"
    }
  }), _vm._ssrNode(" "), _c('ExampleCard', {
    attrs: {
      "index": "2"
    }
  }), _vm._ssrNode(" "), _c('ExampleCard', {
    attrs: {
      "index": "2"
    }
  }), _vm._ssrNode(" "), _c('ExampleCard', {
    attrs: {
      "index": "2"
    }
  })], 2), _vm._ssrNode(" <div class=\"flex flex-wrap items-center justify-center gap-36\" data-v-60cbfc4a><p class=\"flex items-center\" data-v-60cbfc4a>\n              Do you need an architecture service\n              <img" + _vm._ssrAttr("src", __webpack_require__(75)) + " width=\"24\" height=\"24\" alt=\"arrow\" class=\"ml-10\" data-v-60cbfc4a></p> <button class=\"btn px-30 py-15 bg-gray-primary border border-yellow-secondary text-yellow-secondary uppercase hover:bg-yellow-primary hover:text-black-primary font-medium\" data-v-60cbfc4a>Contact us</button></div>")], 2)]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AboutContainer.vue?vue&type=template&id=60cbfc4a&scoped=true&

// EXTERNAL MODULE: ./components/AdvantageCard.vue + 4 modules
var AdvantageCard = __webpack_require__(50);

// EXTERNAL MODULE: ./components/ExampleCard.vue + 4 modules
var ExampleCard = __webpack_require__(51);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AboutContainer.vue?vue&type=script&lang=js&


/* harmony default export */ var AboutContainervue_type_script_lang_js_ = ({
  name: "AboutContainer",
  components: {
    AdvantageCard: AdvantageCard["default"],
    ExampleCard: ExampleCard["default"]
  },
  props: {
    name: {
      // required: true,
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/AboutContainer.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AboutContainervue_type_script_lang_js_ = (AboutContainervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AboutContainer.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(76)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AboutContainervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "60cbfc4a",
  "9a25aef8"
  
)

/* harmony default export */ var AboutContainer = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {AdvantageCard: __webpack_require__(50).default,ExampleCard: __webpack_require__(51).default})


/***/ }),

/***/ 75:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAGlSURBVHgB7dsxbsJAEEbhsQWipaSLr5AbkBNwBHK13IAbJEchFa1bKrK/ZBQrMkmRFTN43tdgQYPmSV5rrTUDAAAAAAAAAAAAAAAAAAAAUEVjSayL1Wq11/X5fD70ff9pAaQIUGbfleG/l8tu+OpYImwjRGgtgTL8nX0PXxTko4R5MmcpAtwQIkKKAOV281Y+jhM/uUdIEaDc6/umaV4sYIQ0T0Gy2Wy6y+UyXozHXBbmVAEkWoR0ASRShJQBJEqEtAEkQoTUAcQ7QvoA4hmBAAOvCAQY8YhAgB/uHaFagOt+e/nza3t8Xdm6eL3xW9UIVQJM7LfPnSI8a4/J/qnKZtzEfvvcdcvlcm8VZH4fEEKVAL/st8/VcbFYHKyCaovw8PSwy7AIt227PZ1OcRbhOfnjgaLq8IUAI/cevhBg4DF8IYD5DV/SB/AcvqQO4D18SRsgwvAlZYAow5d0ASINX1IFiDZ8yXQ+INzwJev5gDG34UvW8wFXrsOXzO8D3IcvWc8HhBi+pFmEr+8rdK0gNd7nAgAAAAAAAAAAAAAAAAAAAAjkC9KcJmKaeks1AAAAAElFTkSuQmCC"

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutContainer_vue_vue_type_style_index_0_id_60cbfc4a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(54);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutContainer_vue_vue_type_style_index_0_id_60cbfc4a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutContainer_vue_vue_type_style_index_0_id_60cbfc4a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutContainer_vue_vue_type_style_index_0_id_60cbfc4a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutContainer_vue_vue_type_style_index_0_id_60cbfc4a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".styleGrid[data-v-60cbfc4a]:first-child,.styleGrid[data-v-60cbfc4a]:nth-child(4n),.styleGrid[data-v-60cbfc4a]:nth-child(4n-1){grid-column:1/3;background-color:red;padding:40px 0}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=about-container.js.map