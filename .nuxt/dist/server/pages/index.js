exports.ids = [15,4,10,11,12,13];
exports.modules = Array(42).concat([
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(47);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("680cbee8", content, true, context)
};

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ScrollLoop.vue?vue&type=template&id=372e74b2&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "flex items-center justify-center relative"
  }, [_vm._ssrNode("<img" + _vm._ssrAttr("src", __webpack_require__(44)) + " width=\"150\" height=\"147\" alt=\"scrollDown\" class=\"shrink-0 scroll-loop\" data-v-372e74b2> <img" + _vm._ssrAttr("src", __webpack_require__(45)) + " width=\"25\" height=\"44\" alt=\"arrow\" class=\"animate-bounce absolute\" data-v-372e74b2>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ScrollLoop.vue?vue&type=template&id=372e74b2&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ScrollLoop.vue?vue&type=script&lang=js&
/* harmony default export */ var ScrollLoopvue_type_script_lang_js_ = ({
  name: 'ScrollLoop'
});
// CONCATENATED MODULE: ./components/ScrollLoop.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ScrollLoopvue_type_script_lang_js_ = (ScrollLoopvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ScrollLoop.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(46)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ScrollLoopvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "372e74b2",
  "207becbe"
  
)

/* harmony default export */ var ScrollLoop = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Element.472d62a.png";

/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAzCAYAAACAArhKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAC7SURBVHgB7djRCYQwEATQ5Sq4DrxOLMXr5ErzOrEES4gTBJEQk92N5msGFhGSeSj5iogzIYQPZhJnXuLPgPmKMy1wUwgTJkyYMGHChAkTJkyYMGHCgHGd8MYM0jHRjF88Yv69cDg/PObjBbNYcKwdMbMYknWsuBUu9ltwC6zq1eJa2PQnNYs1sOfsVDfVYBeq2VyCm9BayRV8C1oqy8G3olelKfwImis/w4+iGXyKcBc0wdewpw+a4G50AxBbgTB1iJlnAAAAAElFTkSuQmCC"

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(42);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".scroll-loop[data-v-372e74b2]{width:150px;height:147px;-webkit-animation:loopRotate-372e74b2 35s linear infinite;animation:loopRotate-372e74b2 35s linear infinite}@-webkit-keyframes loopRotate-372e74b2{0%{transform:rotate(0)}to{transform:rotate(1turn)}}@keyframes loopRotate-372e74b2{0%{transform:rotate(0)}to{transform:rotate(1turn)}}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(69);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("6c8bfe55", content, true, context)
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("312dc31a", content, true, context)
};

/***/ }),
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/StatisticCard.vue?vue&type=template&id=6ee27786&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    class: `StaticCard shrink-0 w-${_vm.width}  h-${_vm.width} border-4 rounded-full border-transparent ${_vm.bgColor}`
  }, [_vm._ssrNode("<div" + _vm._ssrClass(null, `w-full h-full rounded-full flex flex-col items-center px-45 justify-center shrink-0 ${_vm.color}`) + " data-v-6ee27786><h3 class=\"font-bold text-vll font-mak\" data-v-6ee27786>" + _vm._ssrEscape(_vm._s(_vm.width)) + "</h3> <p class=\"line-clamp-4 text-base max-w-202 text-center\" data-v-6ee27786>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum</p></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/StatisticCard.vue?vue&type=template&id=6ee27786&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/StatisticCard.vue?vue&type=script&lang=js&
/* harmony default export */ var StatisticCardvue_type_script_lang_js_ = ({
  name: "StatisticCard",
  props: {
    color: {
      required: true,
      type: String
    },
    bgColor: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      width: '322'
    };
  },

  methods: {
    CardWidth() {
      const element = document.querySelector('.StaticCard');
      this.width = element.scrollWidth; // offsetWidth  offsetHeight clientWidth

      const elements = document.querySelectorAll('.StaticCard');
      elements.forEach(card => {
        card.style.height = `${this.width + 8}px`;
        card.style.width = `${this.width + 8}px`;
      });
      console.log(this.width);
      setTimeout(() => {
        this.CardWidth();
      }, 100);
    }

  },

  mounted() {
    this.CardWidth();
  }

});
// CONCATENATED MODULE: ./components/StatisticCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_StatisticCardvue_type_script_lang_js_ = (StatisticCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/StatisticCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(71)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_StatisticCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6ee27786",
  "db3f28c0"
  
)

/* harmony default export */ var StatisticCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 63 */,
/* 64 */,
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/house3.bb28202.png";

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/time.affa2e5.png";

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/time2.0277f32.png";

/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(52);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".card:hover .btn{color:#000;background-color:#ffda79}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 70 */,
/* 71 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(53);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".statistics__item[data-v-6ee27786]{background-size:100% 100%,50% 50%,50% 50%,50% 50%,50% 50%;background-repeat:no-repeat;background-position:50%,0 0,100% 0,0 100%,100% 100%;background-origin:content-box,border-box,border-box,border-box,border-box;background-clip:content-box,border-box,border-box,border-box,border-box}.statistics__item[data-v-6ee27786]:nth-child(3n-2){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#282828 36%,#282828 0),linear-gradient(10deg,#ffda79,#ffda79 0),linear-gradient(300deg,#282828 36%,#282828 0),linear-gradient(210deg,#ffda79 60%,#282828 0)}.statistics__item[data-v-6ee27786]:nth-child(3n-1){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#282828 36%,#282828 0),linear-gradient(10deg,#ffda79,#282828 0),linear-gradient(300deg,#ffda79 66%,#ffda79 0),linear-gradient(210deg,#282828 60%,#ffda79 0)}.statistics__item[data-v-6ee27786]:nth-child(3n){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#ffda79 36%,#ffda79 0),linear-gradient(10deg,#ffda79,#282828 0),linear-gradient(300deg,#282828 36%,#ffda79 0),linear-gradient(210deg,#282828 60%,#282828 0)}.statistics__item2[data-v-6ee27786]{background-size:100% 100%,50% 50%,50% 50%,50% 50%,50% 50%;background-repeat:no-repeat;background-position:50%,0 0,100% 0,0 100%,100% 100%;background-origin:content-box,border-box,border-box,border-box,border-box;background-clip:content-box,border-box,border-box,border-box,border-box}.statistics__item2[data-v-6ee27786]:nth-child(3n-2){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(10deg,#ffda79,#ffda79 0),linear-gradient(300deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(210deg,#ffda79 60%,#f4f4f4 0)}.statistics__item2[data-v-6ee27786]:nth-child(3n-1){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(10deg,#ffda79,#f4f4f4 0),linear-gradient(300deg,#ffda79 66%,#ffda79 0),linear-gradient(210deg,#f4f4f4 60%,#ffda79 0)}.statistics__item2[data-v-6ee27786]:nth-child(3n){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#ffda79 36%,#ffda79 0),linear-gradient(10deg,#ffda79,#f4f4f4 0),linear-gradient(300deg,#f4f4f4 36%,#ffda79 0),linear-gradient(210deg,#f4f4f4 60%,#f4f4f4 0)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(95);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("27cfbe70", content, true, context)
};

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(100);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("33a9beb0", content, true, context)
};

/***/ }),
/* 80 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ArticleCard.vue?vue&type=template&id=27605336&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "card w-full border border-white-secondary relative cursor-pointer",
    class: _vm.color
  }, [_vm._ssrNode("<img" + _vm._ssrAttr("src", __webpack_require__(65)) + " alt=\"author\" class=\"w-full h-292 object-cover object-center\"> <div" + _vm._ssrClass("p-30", [_vm.color == 'bg-black-primary' ? 'text-white-primary' : 'text-black-primary']) + "><div class=\"flex items-center gap-5 mb-10\">" + (_vm.color === 'bg-black-primary' ? "<img" + _vm._ssrAttr("src", __webpack_require__(66)) + " width=\"18\" height=\"18\" alt=\"time\">" : "<!---->") + " " + (_vm.color === 'bg-white-primary' ? "<img" + _vm._ssrAttr("src", __webpack_require__(67)) + " width=\"18\" height=\"18\" alt=\"time\">" : "<!---->") + " <span>about 10 hours ago</span></div> <h3 class=\"font-bold text-lg line-clamp-4 mb-30\">When the Architect Designs for Communities: 9 Popular Residential D...</h3> <button" + _vm._ssrClass("btn px-30 py-15 text-sm font-medium text-yellow-secondary border border-yellow-secondary", [_vm.color == 'bg-black-primary' ? '' : 'text-gray-primary']) + ">Read more</button></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ArticleCard.vue?vue&type=template&id=27605336&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ArticleCard.vue?vue&type=script&lang=js&
/* harmony default export */ var ArticleCardvue_type_script_lang_js_ = ({
  name: "ArticlesCard",
  props: {
    color: {
      required: true,
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/ArticleCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ArticleCardvue_type_script_lang_js_ = (ArticleCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ArticleCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(68)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ArticleCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "27883b46"
  
)

/* harmony default export */ var ArticleCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServicesCard_vue_vue_type_style_index_0_id_855230ba_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(78);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServicesCard_vue_vue_type_style_index_0_id_855230ba_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServicesCard_vue_vue_type_style_index_0_id_855230ba_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServicesCard_vue_vue_type_style_index_0_id_855230ba_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServicesCard_vue_vue_type_style_index_0_id_855230ba_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(20);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(96);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".card[data-v-855230ba]{background:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat 0 0;background-size:cover}.card:hover .btn[data-v-855230ba]{color:#000;background-color:#ffda79}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/servicesCardBg.39b5b2d.png";

/***/ }),
/* 97 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAEUSURBVHgB7dvdSQQxFIbhM1ZgKbZgBdqBWIE1WcFqBWMHWoodxCwGxJ/1agbysc8D52J3Ll8SBjKpAgAAAAAAAAAAAAAAAAAgRGvtss/aPh2Ov4s5jViv7buHoi5qMmMlrX2ufjx6L+ZyYmW18Z8tcSZiBREriFhBxAoiVhCxgogVRKwgYgURK4hYQcQKIlYQsfaz1Mba6fOsc/PU535Zlk3P8fY4wLwpsY5u+9zVxvYIZsv7svkp+R7BHvu8FS99niuBl45AogUSLZBogUQLJFog0QKJFki0QKIFEi2QaIFECyRaINEC/RPNHeea8I7z+Abiun4fgrrjPLOx0g5jda22RAAAAAAAAAAAAAAAAAAA/vYBhv/fj0VlHVUAAAAASUVORK5CYII="

/***/ }),
/* 98 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAEQSURBVHgB7dvRacMwFIZRuRN0lI7QTtARMlsncDuBR0hH6QaqRAwh8atN9JNz4D74zfChi8F2KQAAAAAAAAAAAAAAAAAAsFFrfW2z1Iu5XxfG1QKd6q3zqNFeCt19nLc2i5M2qHUlnuvWsCft6YkWSLRAogUSLZBogUQLJFog0QKJFki0QKIFEi2QaIEeEW0qO1tvdG7zXp7bb5uPaZr+yo6OeIH5WcTq+kvQU9nZEcHs76tdT1d3RLCvNt+FvhJ/CuPwpBhErCBiBREriFhBxAoiVhCxgogVRKwgYgURK4hYQcQKIlaYuv3HedhY/nG+uA9zyPcY7GRdifN6shZrEAAAAAAAAAAAAAAAAAAgyT8vL92PT9hqxAAAAABJRU5ErkJggg=="

/***/ }),
/* 99 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(79);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwiperNext_vue_vue_type_style_index_0_id_5bcd429e_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".btn[data-v-5bcd429e]{transition:.4s;cursor:pointer}.box:hover .btn[data-v-5bcd429e]{color:#171717;transition:.4s;-webkit-backdrop-filter:blur(10px);backdrop-filter:blur(10px);background-color:transparent}.opacityImg[data-v-5bcd429e]{background:rgba(0,0,0,.8)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 101 */,
/* 102 */,
/* 103 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ServicesCard.vue?vue&type=template&id=855230ba&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "card w-full h-500 md:h-483 p-20 border border-t-yellow-primary relative cursor-pointer overflow-hidden"
  }, [_vm._ssrNode("<div class=\"p-10 md:p-20 lg:p-30 pt-60 flex flex-col text-white-primary w-full h-full\" data-v-855230ba><div class=\"flex-auto\" data-v-855230ba><h3 class=\"font-bold text-xxl mb-15\" data-v-855230ba>Architecture</h3> <p class=\"line-clamp-4 text-base\" data-v-855230ba>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum</p></div> <div data-v-855230ba><button class=\"btn px-30 py-15 text-sm font-medium text-yellow-secondary border border-yellow-secondary inline-block\" data-v-855230ba>Read more</button></div></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ServicesCard.vue?vue&type=template&id=855230ba&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ServicesCard.vue?vue&type=script&lang=js&
/* harmony default export */ var ServicesCardvue_type_script_lang_js_ = ({
  name: "ServicesCard" //   props: {
  //     color: {
  //       required: true,
  //       type: String,
  //     },
  //   },

});
// CONCATENATED MODULE: ./components/ServicesCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ServicesCardvue_type_script_lang_js_ = (ServicesCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ServicesCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(94)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ServicesCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "855230ba",
  "cee91ac4"
  
)

/* harmony default export */ var ServicesCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 104 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/SwiperNext.vue?vue&type=template&id=5bcd429e&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "grid lg:grid-cols-3 gap-30"
  }, [_vm._ssrNode("<div class=\"box lg:col-span-2 relative\" data-v-5bcd429e><p class=\"btn absolute hidden md:block top-15 right-15 px-30 py-15 border-2 font-semibold bg-black-primary text-yellow-primary border-yellow-secondary\" data-v-5bcd429e>Read more</p> <div class=\"w-full h-381 overflow-hidden\" data-v-5bcd429e>" + _vm._ssrList(_vm.images, function (img, index) {
    return index == _vm.count ? "<img" + _vm._ssrAttr("src", img.src) + " alt=\"img\" class=\"w-full h-full object-cover object-center\" data-v-5bcd429e>" : "<!---->";
  }) + "</div> <div class=\"p-30 bg-black-primary\" data-v-5bcd429e><h3 class=\"font-bold text-lg md:text-xl lg:text-xxl mb-15\" data-v-5bcd429e>Mirabad avenue</h3> <p class=\"line-clamp-2\" data-v-5bcd429e>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi, elementum rhoncus. Accumsan cursus eu </p></div></div> <div class=\"flex flex-col items-center justify-center\" data-v-5bcd429e><div class=\"w-full\" data-v-5bcd429e><div class=\"flex gap-15 mb-30\" data-v-5bcd429e><div class=\"p-10 border border-white-primary cursor-pointer\" data-v-5bcd429e><img" + _vm._ssrAttr("src", __webpack_require__(97)) + " width=\"27\" height=\"27\" alt=\"left\" data-v-5bcd429e></div> <div class=\"p-10 border border-white-primary cursor-pointer\" data-v-5bcd429e><img" + _vm._ssrAttr("src", __webpack_require__(98)) + " width=\"27\" height=\"27\" alt=\"right\" data-v-5bcd429e></div></div> <div class=\"w-full h-180 bg-white-primary overflow-hidden relative hidden lg:block\" data-v-5bcd429e><div class=\"opacityImg absolute top-0 left-0 w-full h-full\" data-v-5bcd429e></div> " + _vm._ssrList(_vm.images, function (img, index) {
    return index == _vm.active ? "<img" + _vm._ssrAttr("src", img.src) + " alt=\"img\" class=\"w-full h-full object-cover object-center\" data-v-5bcd429e>" : "<!---->";
  }) + "</div></div></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/SwiperNext.vue?vue&type=template&id=5bcd429e&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/SwiperNext.vue?vue&type=script&lang=js&
/* harmony default export */ var SwiperNextvue_type_script_lang_js_ = ({
  name: "MainSwiperNext",

  data() {
    return {
      count: 0,
      active: 1,
      images: [{
        id: 1,
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/1121098-pink-nature-wallpaper-1920x1080-lockscreen.jpg/1024px-1121098-pink-nature-wallpaper-1920x1080-lockscreen.jpg'
      }, {
        id: 2,
        src: 'https://images4.alphacoders.com/378/37864.jpg'
      }, {
        id: 3,
        src: 'https://wallpaper.dog/large/10950083.jpg'
      }, {
        id: 4,
        src: 'https://wallpaperfordesktop.com/wp-content/uploads/2021/11/Nature-Wallpapers.jpg'
      }, {
        id: 5,
        src: 'https://thumbs.dreamstime.com/b/green-leaf-texture-dew-drop-fresh-spring-nature-wallpaper-background-green-leaf-texture-dew-drop-fresh-nature-wallpaper-167543228.jpg'
      }],
      imgLength: 5
    };
  },

  methods: {
    AddActive() {
      if (this.count < this.imgLength - 1) {
        this.count += 1;
      } else {
        this.count = 0;
      }

      if (this.active < this.imgLength - 1) {
        this.active += 1;
      } else {
        this.active = 0;
      }
    },

    RemoveActive() {
      if (this.count > 0) {
        this.count -= 1;
      } else {
        this.count = this.imgLength - 1;
      }

      if (this.active > 0) {
        this.active -= 1;
      } else {
        this.active = this.imgLength - 1;
      }
    }

  }
});
// CONCATENATED MODULE: ./components/SwiperNext.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_SwiperNextvue_type_script_lang_js_ = (SwiperNextvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/SwiperNext.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(99)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_SwiperNextvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "5bcd429e",
  "7b94f51b"
  
)

/* harmony default export */ var SwiperNext = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 105 */,
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(113);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("3f0ac1c1", content, true, context)
};

/***/ }),
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/dreamArch.21845a8.png";

/***/ }),
/* 112 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3b354068_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(106);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3b354068_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3b354068_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3b354068_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3b354068_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(20);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(114);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".main[data-v-3b354068]{filter:invert(6%);height:calc(100vh - 106.91px);background:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat bottom;background-size:cover}.top__70[data-v-3b354068]{top:-100px}@media screen and (max-width:768px){.main[data-v-3b354068]{height:calc(100vh - 96.91px);background:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat bottom;background-size:cover}}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/MainBg.f8805e7.jpg";

/***/ }),
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=template&id=3b354068&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "relative"
  }, [_vm._ssrNode("<div class=\"main max-w-1520 mx-auto flex items-center justify-center text-white-primary\" data-v-3b354068>", "</div>", [_vm._ssrNode("<div class=\"container py-60 relative lg:flex items-end\" data-v-3b354068>", "</div>", [_vm._ssrNode("<div class=\"flex flex-col flex-auto items-center justify-center md:py-40 relative\" data-v-3b354068>", "</div>", [_vm._ssrNode("<h1 class=\"font-bold font-mak inline-block uppercase text-Title md:text-mdTitle lg:text-lgTitle max-w-900 text-center leading-38 md:leading-60 mb-15\" data-v-3b354068>Modern architectural solutions</h1> <p class=\"text-lg leading-30 font-light max-w-600 text-center mb-30 mx-auto\" data-v-3b354068>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum, </p> "), _c('nuxt-link', {
    staticClass: "z-10",
    attrs: {
      "to": "/about"
    }
  }, [_c('button', {
    staticClass: "btn px-30 py-15 mb-60 md:m-20 inline-block text-sm font-medium uppercase transition-all text-black-primary border border-yellow-secondary bg-yellow-primary hover:text-yellow-primary hover:bg-transparent cursor-pointer p-40"
  }, [_vm._v("About us")])]), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"shrink-0 flex w-full justify-end lg:absolute bottom-0 right-0\" data-v-3b354068>", "</div>", [_c('ScrollLoop')], 1)], 2)])]), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"bg-black-secondary space-y-120 md:space-y-180 lg:space-y-240 py-120\" data-v-3b354068>", "</div>", [_vm._ssrNode("<div class=\"relative max-w-1520 w-full mx-auto\" data-v-3b354068>", "</div>", [_vm._ssrNode("<p class=\"absoluteText absolute top-70 left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-secondary\" data-v-3b354068>SeRvices</p> "), _vm._ssrNode("<div class=\"container text-white-primary space-y-60\" data-v-3b354068>", "</div>", [_vm._ssrNode("<h2 class=\"font-medium text-Title md:text-mdTitle lg:text-lgTitle uppercase z-10\" data-v-3b354068><span class=\"text-yellow-primary\" data-v-3b354068>Services</span> offered by our team are provided by their <span class=\"text-yellow-primary\" data-v-3b354068>masters</span></h2> "), _vm._ssrNode("<div class=\"grid md:grid-cols-2 lg:grid-cols-3 gap-30\" data-v-3b354068>", "</div>", [_c('ServicesCard'), _vm._ssrNode(" "), _c('ServicesCard'), _vm._ssrNode(" "), _c('ServicesCard')], 2)], 2)], 2), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"relative max-w-1520 w-full mx-auto divBox1\" data-v-3b354068>", "</div>", [_vm._ssrNode("<p class=\"absoluteText absolute top-70 left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-secondary\" data-v-3b354068>Statistics</p> "), _vm._ssrNode("<div class=\"container text-white-primary space-y-60 z-10\" data-v-3b354068>", "</div>", [_vm._ssrNode("<h2 class=\"font-medium text-Title md:text-mdTitle lg:text-lgTitle uppercase z-10\" data-v-3b354068>these short <span class=\"text-yellow-primary\" data-v-3b354068>statistics</span> will replace many commendations</h2> "), _vm._ssrNode("<div class=\"grid md:grid-cols-2 lg:grid-cols-3 gap-30\" data-v-3b354068>", "</div>", [_c('StatisticCard', {
    attrs: {
      "color": "bg-gray-secondary text-white-primary",
      "bgColor": "statistics__item"
    }
  }), _vm._ssrNode(" "), _c('StatisticCard', {
    attrs: {
      "color": "bg-gray-secondary text-white-primary",
      "bgColor": "statistics__item"
    }
  }), _vm._ssrNode(" "), _c('StatisticCard', {
    attrs: {
      "color": "bg-gray-secondary text-white-primary",
      "bgColor": "statistics__item"
    }
  })], 2)], 2)], 2), _vm._ssrNode(" <div class=\"relative max-w-1520 w-full mx-auto divBox2\" data-v-3b354068><p class=\"absoluteText top__70 absolute left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-secondary\" data-v-3b354068>About us</p> <div class=\"container\" data-v-3b354068><div class=\"grid md:grid-cols-2 bg-black-primary border-2 border-gray-secondary z-10\" data-v-3b354068><div class=\"max-h-335 md:max-h-513 w-full overflow-hidden relative\" data-v-3b354068><div class=\"absolute top-0 bottom-0 left-0 w-full -rotate-90 overflow-hidden\" data-v-3b354068><p class=\"font-medium p-10 break-normal bg-yellow-primary h-40 overflow-hidden\" data-v-3b354068>ABOUT US  ·  OUR TEAM  ·  ABOUT US  ·  OUR TEAM  ·  ABOUT US  ·  OUR TEAM</p></div> <img" + _vm._ssrAttr("src", __webpack_require__(111)) + " alt class=\"w-full h-full object-cover object-center\" data-v-3b354068></div> <div class=\"p-30 md:py-40 flex flex-col gap-30 text-white-primary\" data-v-3b354068><div class=\"flex-auto\" data-v-3b354068><h3 class=\"font-bold text-xxl mb-15 md:mb-30\" data-v-3b354068>Brief about our team</h3> <p data-v-3b354068>Volutpat cras consectetur tellus congue tellus sapien, facilisis tortor. Nulla tristique pellentesque nisi nisi, elementum rhoncus. Accumsan cursus eu egestas fermentum augue. Pharetra sem tempor rutrum viverra amet ullamcorper fringilla aliquet. Ipsum velit sit nunc, habitant turpis scelerisque nulla aliquam eu deliranbum.</p></div> <div data-v-3b354068><button class=\"btn px-30 py-15 text-sm font-medium text-yellow-secondary border border-yellow-secondary inline-block\" data-v-3b354068>Read more</button></div></div></div></div></div> "), _vm._ssrNode("<div class=\"relative max-w-1520 w-full mx-auto divBox3\" data-v-3b354068>", "</div>", [_vm._ssrNode("<p class=\"absoluteText absolute top-70 left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-secondary\" data-v-3b354068>PORTFOLIO</p> "), _vm._ssrNode("<div class=\"container text-white-primary space-y-60 z-10\" data-v-3b354068>", "</div>", [_vm._ssrNode("<h2 class=\"font-medium text-Title md:text-mdTitle lg:text-lgTitle uppercase z-10\" data-v-3b354068>Collection of top <span class=\"text-yellow-primary\" data-v-3b354068>projects</span> implemented by <span class=\"text-yellow-primary\" data-v-3b354068>our team</span></h2> "), _c('SwiperNext')], 2)], 2), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"relative max-w-1520 w-full mx-auto divBox4\" data-v-3b354068>", "</div>", [_vm._ssrNode("<p class=\"absoluteText absolute top-70 left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-secondary\" data-v-3b354068>BLOG</p> "), _vm._ssrNode("<div class=\"container text-white-primary space-y-30 z-10\" data-v-3b354068>", "</div>", [_vm._ssrNode("<h2 class=\"font-medium text-Title md:text-mdTitle lg:text-lgTitle uppercase z-10 mb-30\" data-v-3b354068>List of best <span class=\"text-yellow-primary\" data-v-3b354068>articles</span> published by <span class=\"text-yellow-primary\" data-v-3b354068>our team</span></h2> "), _vm._ssrNode("<div class=\"grid md:grid-cols-2 lg:grid-cols-3 gap-30\" data-v-3b354068>", "</div>", [_c('ArticleCard', {
    attrs: {
      "color": "bg-black-primary"
    }
  }), _vm._ssrNode(" "), _c('ArticleCard', {
    attrs: {
      "color": "bg-black-primary"
    }
  }), _vm._ssrNode(" "), _c('ArticleCard', {
    attrs: {
      "color": "bg-black-primary"
    }
  })], 2), _vm._ssrNode(" <button class=\"btn px-30 py-15 inline-block text-sm font-medium text-yellow-secondary border border-yellow-secondary hover:bg-yellow-primary hover:text-black-primary\" data-v-3b354068>All our articles</button>")], 2)], 2)], 2)], 2);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./pages/index.vue?vue&type=template&id=3b354068&scoped=true&

// EXTERNAL MODULE: ./components/ArticleCard.vue + 4 modules
var ArticleCard = __webpack_require__(80);

// EXTERNAL MODULE: ./components/ServicesCard.vue + 4 modules
var ServicesCard = __webpack_require__(103);

// EXTERNAL MODULE: ./components/StatisticCard.vue + 4 modules
var StatisticCard = __webpack_require__(62);

// EXTERNAL MODULE: ./components/TheHeader.vue + 4 modules
var TheHeader = __webpack_require__(7);

// EXTERNAL MODULE: ./components/ScrollLoop.vue + 4 modules
var ScrollLoop = __webpack_require__(43);

// EXTERNAL MODULE: ./components/SwiperNext.vue + 4 modules
var SwiperNext = __webpack_require__(104);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=script&lang=js&






/* harmony default export */ var lib_vue_loader_options_pagesvue_type_script_lang_js_ = ({
  name: 'IndexPage',
  components: {
    ArticleCard: ArticleCard["default"],
    ServicesCard: ServicesCard["default"],
    StatisticCard: StatisticCard["default"],
    TheHeader: TheHeader["default"],
    ScrollLoop: ScrollLoop["default"],
    SwiperNext: SwiperNext["default"]
  }
});
// CONCATENATED MODULE: ./pages/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var pagesvue_type_script_lang_js_ = (lib_vue_loader_options_pagesvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./pages/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(112)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pagesvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3b354068",
  "cfdd8644"
  
)

/* harmony default export */ var pages = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ScrollLoop: __webpack_require__(43).default,ServicesCard: __webpack_require__(103).default,StatisticCard: __webpack_require__(62).default,SwiperNext: __webpack_require__(104).default,ArticleCard: __webpack_require__(80).default})


/***/ })
]);;
//# sourceMappingURL=index.js.map