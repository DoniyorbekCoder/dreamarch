exports.ids = [14,1,4,5,6,8,9,10,12];
exports.modules = Array(42).concat([
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(47);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("680cbee8", content, true, context)
};

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ScrollLoop.vue?vue&type=template&id=372e74b2&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "flex items-center justify-center relative"
  }, [_vm._ssrNode("<img" + _vm._ssrAttr("src", __webpack_require__(44)) + " width=\"150\" height=\"147\" alt=\"scrollDown\" class=\"shrink-0 scroll-loop\" data-v-372e74b2> <img" + _vm._ssrAttr("src", __webpack_require__(45)) + " width=\"25\" height=\"44\" alt=\"arrow\" class=\"animate-bounce absolute\" data-v-372e74b2>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ScrollLoop.vue?vue&type=template&id=372e74b2&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ScrollLoop.vue?vue&type=script&lang=js&
/* harmony default export */ var ScrollLoopvue_type_script_lang_js_ = ({
  name: 'ScrollLoop'
});
// CONCATENATED MODULE: ./components/ScrollLoop.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ScrollLoopvue_type_script_lang_js_ = (ScrollLoopvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ScrollLoop.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(46)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ScrollLoopvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "372e74b2",
  "207becbe"
  
)

/* harmony default export */ var ScrollLoop = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Element.472d62a.png";

/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAzCAYAAACAArhKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAC7SURBVHgB7djRCYQwEATQ5Sq4DrxOLMXr5ErzOrEES4gTBJEQk92N5msGFhGSeSj5iogzIYQPZhJnXuLPgPmKMy1wUwgTJkyYMGHChAkTJkyYMGHCgHGd8MYM0jHRjF88Yv69cDg/PObjBbNYcKwdMbMYknWsuBUu9ltwC6zq1eJa2PQnNYs1sOfsVDfVYBeq2VyCm9BayRV8C1oqy8G3olelKfwImis/w4+iGXyKcBc0wdewpw+a4G50AxBbgTB1iJlnAAAAAElFTkSuQmCC"

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(42);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ScrollLoop_vue_vue_type_style_index_0_id_372e74b2_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".scroll-loop[data-v-372e74b2]{width:150px;height:147px;-webkit-animation:loopRotate-372e74b2 35s linear infinite;animation:loopRotate-372e74b2 35s linear infinite}@-webkit-keyframes loopRotate-372e74b2{0%{transform:rotate(0)}to{transform:rotate(1turn)}}@keyframes loopRotate-372e74b2{0%{transform:rotate(0)}to{transform:rotate(1turn)}}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 48 */,
/* 49 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/MainComponent.vue?vue&type=template&id=06846683&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "bg-black-primary"
  }, [_vm._ssrNode("<div class=\"container\">", "</div>", [_vm._ssrNode("<div class=\"flex flex-col md:flex-row items-center md:justify-between gap-y-60 gap-x-20 py-60 md:py-80\">", "</div>", [_vm._ssrNode("<div>", "</div>", [_vm._ssrNode("<h1" + _vm._ssrClass("text-Title md:text-mdTitle lg:text-lgTitle uppercase font-mak font-semibold text-white-primary", _vm.width) + ">" + _vm._s(_vm.title) + "</h1> "), _vm._ssrNode("<div class=\"flex items-center text-base gap-5 text-white-primary\">", "</div>", [_c('nuxt-link', {
    attrs: {
      "to": "/"
    }
  }, [_vm._v("Main page / ")]), _vm._ssrNode(" <p class=\"font-semibold\">" + _vm._ssrEscape(_vm._s(_vm.page)) + "</p>")], 2)], 2), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"flex justify-end ml-auto\">", "</div>", [_c('ScrollLoop')], 1)], 2)])]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/MainComponent.vue?vue&type=template&id=06846683&

// EXTERNAL MODULE: ./components/ScrollLoop.vue + 4 modules
var ScrollLoop = __webpack_require__(43);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/MainComponent.vue?vue&type=script&lang=js&

/* harmony default export */ var MainComponentvue_type_script_lang_js_ = ({
  name: 'MainComponent',
  props: {
    title: {
      required: true,
      type: String
    },
    width: {
      required: true,
      type: String
    },
    page: {
      required: true,
      type: String
    }
  },
  components: {
    ScrollLoop: ScrollLoop["default"]
  }
});
// CONCATENATED MODULE: ./components/MainComponent.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_MainComponentvue_type_script_lang_js_ = (MainComponentvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/MainComponent.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_MainComponentvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "851e7f38"
  
)

/* harmony default export */ var MainComponent = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {ScrollLoop: __webpack_require__(43).default})


/***/ }),
/* 50 */,
/* 51 */,
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(69);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("6c8bfe55", content, true, context)
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("312dc31a", content, true, context)
};

/***/ }),
/* 54 */,
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/i.8ed363a.png";

/***/ }),
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/author.c4e7c34.jpg";

/***/ }),
/* 62 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/StatisticCard.vue?vue&type=template&id=6ee27786&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    class: `StaticCard shrink-0 w-${_vm.width}  h-${_vm.width} border-4 rounded-full border-transparent ${_vm.bgColor}`
  }, [_vm._ssrNode("<div" + _vm._ssrClass(null, `w-full h-full rounded-full flex flex-col items-center px-45 justify-center shrink-0 ${_vm.color}`) + " data-v-6ee27786><h3 class=\"font-bold text-vll font-mak\" data-v-6ee27786>" + _vm._ssrEscape(_vm._s(_vm.width)) + "</h3> <p class=\"line-clamp-4 text-base max-w-202 text-center\" data-v-6ee27786>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam enim nunc auctor diam porttitor malesuada tortor ipsum</p></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/StatisticCard.vue?vue&type=template&id=6ee27786&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/StatisticCard.vue?vue&type=script&lang=js&
/* harmony default export */ var StatisticCardvue_type_script_lang_js_ = ({
  name: "StatisticCard",
  props: {
    color: {
      required: true,
      type: String
    },
    bgColor: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      width: '322'
    };
  },

  methods: {
    CardWidth() {
      const element = document.querySelector('.StaticCard');
      this.width = element.scrollWidth; // offsetWidth  offsetHeight clientWidth

      const elements = document.querySelectorAll('.StaticCard');
      elements.forEach(card => {
        card.style.height = `${this.width + 8}px`;
        card.style.width = `${this.width + 8}px`;
      });
      console.log(this.width);
      setTimeout(() => {
        this.CardWidth();
      }, 100);
    }

  },

  mounted() {
    this.CardWidth();
  }

});
// CONCATENATED MODULE: ./components/StatisticCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_StatisticCardvue_type_script_lang_js_ = (StatisticCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/StatisticCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(71)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_StatisticCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6ee27786",
  "db3f28c0"
  
)

/* harmony default export */ var StatisticCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 63 */,
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(82);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("95b77c48", content, true, context)
};

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/house3.bb28202.png";

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/time.affa2e5.png";

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/time2.0277f32.png";

/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(52);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleCard_vue_vue_type_style_index_0_id_27605336_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".card:hover .btn{color:#000;background-color:#ffda79}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("fad7d56c", content, true, context)
};

/***/ }),
/* 71 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(53);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticCard_vue_vue_type_style_index_0_id_6ee27786_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".statistics__item[data-v-6ee27786]{background-size:100% 100%,50% 50%,50% 50%,50% 50%,50% 50%;background-repeat:no-repeat;background-position:50%,0 0,100% 0,0 100%,100% 100%;background-origin:content-box,border-box,border-box,border-box,border-box;background-clip:content-box,border-box,border-box,border-box,border-box}.statistics__item[data-v-6ee27786]:nth-child(3n-2){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#282828 36%,#282828 0),linear-gradient(10deg,#ffda79,#ffda79 0),linear-gradient(300deg,#282828 36%,#282828 0),linear-gradient(210deg,#ffda79 60%,#282828 0)}.statistics__item[data-v-6ee27786]:nth-child(3n-1){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#282828 36%,#282828 0),linear-gradient(10deg,#ffda79,#282828 0),linear-gradient(300deg,#ffda79 66%,#ffda79 0),linear-gradient(210deg,#282828 60%,#ffda79 0)}.statistics__item[data-v-6ee27786]:nth-child(3n){background-image:linear-gradient(#282828,#282828),linear-gradient(30deg,#ffda79 36%,#ffda79 0),linear-gradient(10deg,#ffda79,#282828 0),linear-gradient(300deg,#282828 36%,#ffda79 0),linear-gradient(210deg,#282828 60%,#282828 0)}.statistics__item2[data-v-6ee27786]{background-size:100% 100%,50% 50%,50% 50%,50% 50%,50% 50%;background-repeat:no-repeat;background-position:50%,0 0,100% 0,0 100%,100% 100%;background-origin:content-box,border-box,border-box,border-box,border-box;background-clip:content-box,border-box,border-box,border-box,border-box}.statistics__item2[data-v-6ee27786]:nth-child(3n-2){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(10deg,#ffda79,#ffda79 0),linear-gradient(300deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(210deg,#ffda79 60%,#f4f4f4 0)}.statistics__item2[data-v-6ee27786]:nth-child(3n-1){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#f4f4f4 36%,#f4f4f4 0),linear-gradient(10deg,#ffda79,#f4f4f4 0),linear-gradient(300deg,#ffda79 66%,#ffda79 0),linear-gradient(210deg,#f4f4f4 60%,#ffda79 0)}.statistics__item2[data-v-6ee27786]:nth-child(3n){background-image:linear-gradient(#f4f4f4,#f4f4f4),linear-gradient(30deg,#ffda79 36%,#ffda79 0),linear-gradient(10deg,#ffda79,#f4f4f4 0),linear-gradient(300deg,#f4f4f4 36%,#ffda79 0),linear-gradient(210deg,#f4f4f4 60%,#f4f4f4 0)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(88);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("4d3ccba0", content, true, context)
};

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(93);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("207f51a7", content, true, context)
};

/***/ }),
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ArticleCard.vue?vue&type=template&id=27605336&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "card w-full border border-white-secondary relative cursor-pointer",
    class: _vm.color
  }, [_vm._ssrNode("<img" + _vm._ssrAttr("src", __webpack_require__(65)) + " alt=\"author\" class=\"w-full h-292 object-cover object-center\"> <div" + _vm._ssrClass("p-30", [_vm.color == 'bg-black-primary' ? 'text-white-primary' : 'text-black-primary']) + "><div class=\"flex items-center gap-5 mb-10\">" + (_vm.color === 'bg-black-primary' ? "<img" + _vm._ssrAttr("src", __webpack_require__(66)) + " width=\"18\" height=\"18\" alt=\"time\">" : "<!---->") + " " + (_vm.color === 'bg-white-primary' ? "<img" + _vm._ssrAttr("src", __webpack_require__(67)) + " width=\"18\" height=\"18\" alt=\"time\">" : "<!---->") + " <span>about 10 hours ago</span></div> <h3 class=\"font-bold text-lg line-clamp-4 mb-30\">When the Architect Designs for Communities: 9 Popular Residential D...</h3> <button" + _vm._ssrClass("btn px-30 py-15 text-sm font-medium text-yellow-secondary border border-yellow-secondary", [_vm.color == 'bg-black-primary' ? '' : 'text-gray-primary']) + ">Read more</button></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/ArticleCard.vue?vue&type=template&id=27605336&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ArticleCard.vue?vue&type=script&lang=js&
/* harmony default export */ var ArticleCardvue_type_script_lang_js_ = ({
  name: "ArticlesCard",
  props: {
    color: {
      required: true,
      type: String
    }
  }
});
// CONCATENATED MODULE: ./components/ArticleCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ArticleCardvue_type_script_lang_js_ = (ArticleCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/ArticleCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(68)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ArticleCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "27883b46"
  
)

/* harmony default export */ var ArticleCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 81 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorCard_vue_vue_type_style_index_0_id_ab593b50_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(64);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorCard_vue_vue_type_style_index_0_id_ab593b50_prod_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorCard_vue_vue_type_style_index_0_id_ab593b50_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorCard_vue_vue_type_style_index_0_id_ab593b50_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorCard_vue_vue_type_style_index_0_id_ab593b50_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".bg{background:hsla(0,0%,9%,.5);-webkit-backdrop-filter:blur(10px);backdrop-filter:blur(10px)}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 83 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(70);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AuthorDescCard_vue_vue_type_style_index_0_id_6053e95a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(20);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(85);
var ___CSS_LOADER_URL_IMPORT_1___ = __webpack_require__(86);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
var ___CSS_LOADER_URL_REPLACEMENT_1___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_1___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "*[data-v-6053e95a]{box-sizing:border-box}.text[data-v-6053e95a]{display:-webkit-box;-webkit-line-clamp:10;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis}.box[data-v-6053e95a]:after{top:15px;left:15px;background:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat 0 0;background-size:15px}.box[data-v-6053e95a]:after,.box[data-v-6053e95a]:before{content:\"\";position:absolute;width:15px;height:12px}.box[data-v-6053e95a]:before{bottom:15px;right:15px;background:url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ") no-repeat 0 0;background-size:15px}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 85 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAwCAYAAABNPhkJAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMQSURBVHgB7ZrLcRpBEIZ/RjyO9o3iVV4isB2BpQhMBsYRWI7AKAOUgRSByUDrCGxHwLqKR3GyfOONu6mFEi6xO8u/i1xov4sezNLzd/f09Axk+v3+ChxepVKpg0Dm8EV+tECwWq2a1Wr1NmycwTMjFXzqpIJPnVTwqZMKPnVSwadOKvjU+S8EZzKZexwJI8Y8PDHL5fIPSIwxL63GgccBiTj9N0jkPGwnWAb+AEm327Uytg+Zwy+QyHs4NuNUsAeSQqHwAgSz2cwDiWSJXYQFDyTitDcgqNfr9zHUEqs5aNGiU1rewwFJDEvLsRlkxuPxT5BIlaUirIjTXJD0er3QeRhNJ/GuCwKZ7DlI4sg0WZ7vQsf4xlxwOIPB4BUI/EyjGhCbTFsLFs+44GmAwM80Nsqhc1gLLpVK38B7lxLsE3qRHoRuTcPhMDCtt52WePcGnLFztgGR/bgD0vGLxSLQ8VvBZ2dnHZDk8/lPIIgprZtBjt8K1rRmq7U8fwkScXwLBJrWQY7fOTxI8aKirMZkL/wAgjjqidDc98KO4MlkcpuksQi0weHsK147gnUNsca0eIVVyjCm0+k1+OLVeuz/5pjGbEnS8eaYxqKQlOPNMY1FISnHmySN9fv99yCIw/HSAd48/NuEGPPA0Wa6L/9ioAUOPdhs92UTZEz25SZIY2z3VS6Xr9mGSKLc2jg+8NbSbwKoZkS7L/boKN3XRxCprQ1RLpfTbwqFX9NKaqsxDweixv5dR1ERx3tsasvzl1rAMjaDdaBM2gWBPH9Rq9VcEEjbesfcrujBxOoi3k/tFg5EvcuKVfzU9nAAa7HGNKwivEG2ma+IdrPhZbPZRrFYpC8KN4xGo9fz+dyVX62rv4hty1n7al2IEQF/PdueVzsy/m2cYhV9P4mU7TFUC12jWq1+9nsLRIqwIuvZkfV4h4B7YE1h3U6QIGHfz9yksMxj52OcyIKVANGxp3AQ+0RrCmtUH3vmoE8PdZsQ711gt4AkksJBVCqVK+wK3qYwkkAjrVvFw9btKVD7Mo/vNg3OX6q4gvFXKpCjAAAAAElFTkSuQmCC"

/***/ }),
/* 86 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAwCAYAAABNPhkJAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAALsSURBVHgB7ZpBbtNAFIb/NwlruAG5Ab0BUIgidukJGm5Qdm1UqWbRZElvQDgBYYVoJJreoD0B5gZBYgE0yfDGYFDTxB7nnxTJ+NvESmzN/O/9895MZIEHp310YPEaBCKImgd4CQKdxyedRwMEBv8ZleCyUwkuO5XgslMJLjuV4LJTCS47leClCO6CRCxi/GssJl6CdbL3QGINJmAhz8KauNgvwwEETy2+gOAs4ueggie+a7gBkm/fcQGCqzv8HKzFhZdgywrWtbMTcZaWGu6DxNUR3wxvgcFw2XXYAC6bGVzmCj49JsXCLR2MQSJzPALJswOMcwVbwQOQTIFzkMxZl8mvoOcK1hvaYND16yILgvd9NIS0dOqy/AyDs5IV3s7G4iFIUpdlCv7YSwai+l8NGIJEJ9kBR5y6LFPwVOiBXLEZg8DZmXWZ2nScXpqsgfTGDqhxMHxyiM8gqAO7INF29Ca9Xik4xLoxBgOQzG04OzuyLB2BHKi5j3cg+NDDLludzYKOOjY0kPr5BDwRGLQlir1eQ8xGBlJqlqvOrkMIv528UUNuCA6U3QFbrGZhgh4tfrcswxE2MFARXHYDtKKlQb8mOER2dWcVsdm9AlfdVcNkVdAXMxyBI67PuMmGCPpccLIq6CbkQK4FsNlFgKC3Mt4lSQQnu6oAAz3t/t3RrMPoGEeh++6S35MNPj1QjTygJ3tmwR4YtFDlBd0E2TMHKFQu6OBOZrFPdzB601sQiJ53W+T7V8kxlAy6nsr2fIJuZoId/YyxHrGZ8UfI7S7O3YtrWBPnsOah375d0otRD6+02RdaQ3WDre19XCIQbnmp485QoJ6ogGGzmyTNiz9tSR968fufhdjnQQ1OJ6RYhx7jYnXcY1Ux8Hwk/voDz1EAWfzCJ8rOQuy6zWPUx5HNLkKx6wxFi6UUHfA2xKZkBH8tsY6VfwC4N18XLX6bYh0rLL62WIfk3ZBGWdds1CJ3UgyJ4+Zo61zaTM//Ccm8Cf5AhLGaAAAAAElFTkSuQmCC"

/***/ }),
/* 87 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(73);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuestionAccordion_vue_vue_type_style_index_0_id_5bc7ee2a_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(20);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(89);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".accordion-body[data-v-5bc7ee2a]{padding-top:0}.accordion-button[data-v-5bc7ee2a]:not(.collapsed):after{background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");transform:rotate(-180deg)}.accordion-button[data-v-5bc7ee2a]:after{flex-shrink:0;width:2rem;height:2rem;margin-left:auto;content:\"\";background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");background-repeat:no-repeat;background-size:2rem;transition:transform .2s ease-in-out}.accordion-button[data-v-5bc7ee2a]:focus{z-index:3;outline:none;border:none;box-shadow:none}.accordion-button[data-v-5bc7ee2a]:not(.collapsed){color:#171717;background-color:transparent;box-shadow:none}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 89 */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAACxLAAAsSwGlPZapAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAI2SURBVHgB7dytbpNRAMbxUzAkmwCxzSKRSC4HJA4cDosbdwC3wCVwO5sAswQ3zpMgYKFZ99Xz9fslzZpWnT7/7K15WwoAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6zk+Pv54cnLyI4+jo6PTsphezv+4NFAPfbrZbD7Up0/yqM9fHRwcPL+4uPhWFlDH/1LP/K78e/5Sz/+97NmmNJDq65+nV1+/vLz8enZ29qZM7M/4r6++Xs/+s579WdmzR6Uj+WDyAZVJbRu/pSYB1No/b3tv1giuGz///UoDTb4D5FqXa379QF7+7/28PtN3gl3GPz8/f1saaBJAZNwVIthl/Jbfe5oFELNH0Pv40TSAmDWCEcaP5gHEbBGMMn50EUDMEsFI40c3AcToEYw2fnQVQIwawYjjR3cBxGgRjDp+dBlAjBLByONHtwFE7xGMPn50HUD0GsEM40f3AURvEcwyfgwRQPQSwUzjxzABROsIZhs/hgogWkUw4/gxXACx7whmHT+GDCD2FcHM48ewAcRDRzD7+DF0APFQEawwfgwfQNx3BKuMH1MEEPcVwUrjxzQBxF0jWG38mCqAuG0EK44f0wUQN41g1fGjyb2B+7Lr3Tirjh9TBxB3uR9vhZtVp7wE/O26y8E2K4wf0wcQN41glfFjiQBi1whWGj+WCSCui2C18WOpAGJbBCuOH8sFEIng8PAwP1Hzoj5+1fE/1fHfFwAAAAAAAAAAAAAAAAAAAAAAAAAAAADYp99ySRW0Oml9MQAAAABJRU5ErkJggg=="

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(91);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("51b2c2d9", content, true)

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".vsa-item__heading{width:100%;height:100%}.vsa-item__heading,.vsa-item__trigger{display:flex;justify-content:flex-start;align-items:center}.vsa-item__trigger{margin:0;padding:0;color:inherit;font-family:inherit;font-size:100%;line-height:1.15;border-width:0;background-color:transparent;background-image:none;overflow:visible;text-transform:none;flex:1 1 auto;color:var(--vsa-text-color);transition:all .2s linear;padding:var(--vsa-heading-padding)}.vsa-item__trigger[role=button]{cursor:pointer}.vsa-item__trigger[type=button],.vsa-item__trigger[type=reset],.vsa-item__trigger[type=submit]{-webkit-appearance:button}.vsa-item__trigger:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}.vsa-item__trigger::-moz-focus-inner,.vsa-item__trigger[type=button]::-moz-focus-inner,.vsa-item__trigger[type=reset]::-moz-focus-inner,.vsa-item__trigger[type=submit]::-moz-focus-inner{border-style:none;padding:0}.vsa-item__trigger:-moz-focusring,.vsa-item__trigger[type=button]:-moz-focusring,.vsa-item__trigger[type=reset]:-moz-focusring,.vsa-item__trigger[type=submit]:-moz-focusring{outline:1px dotted ButtonText}.vsa-item__trigger:focus,.vsa-item__trigger:hover{outline:none;background-color:var(--vsa-highlight-color);color:var(--vsa-bg-color)}.vsa-item__trigger__icon--is-default{width:40px;height:40px;transform:scale(var(--vsa-default-icon-size))}.vsa-item__trigger__icon--is-default:after,.vsa-item__trigger__icon--is-default:before{background-color:var(--vsa-text-color);content:\"\";height:3px;position:absolute;top:10px;transition:all .13333s ease-in-out;width:30px}.vsa-item__trigger__icon--is-default:before{left:0;transform:rotate(45deg) translate3d(8px,22px,0);transform-origin:100%}.vsa-item__trigger__icon--is-default:after{transform:rotate(-45deg) translate3d(-8px,22px,0);right:0;transform-origin:0}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:before{transform:rotate(45deg) translate3d(14px,14px,0)}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:after{transform:rotate(-45deg) translate3d(-14px,14px,0)}.vsa-item__trigger__icon{display:block;margin-left:auto;position:relative;transition:all .2s ease-in-out}.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:before,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:before{background-color:var(--vsa-bg-color)}.vsa-item__trigger__content{font-weight:700;font-size:1.25rem}.vsa-item__content{margin:0;padding:var(--vsa-content-padding)}.vsa-item--is-active .vsa-item__heading,.vsa-item:not(:last-of-type){border-bottom:var(--vsa-border)}.vsa-collapse-enter-active,.vsa-collapse-leave-active{transition-property:opacity,height,padding-top,padding-bottom;transition-duration:.3s;transition-timing-function:ease-in-out}.vsa-collapse-enter,.vsa-collapse-leave-active{opacity:0;height:0;padding-top:0;padding-bottom:0;overflow:hidden}.vsa-list{--vsa-max-width:720px;--vsa-min-width:300px;--vsa-heading-padding:1rem 1rem;--vsa-text-color:#373737;--vsa-highlight-color:#57a;--vsa-bg-color:#fff;--vsa-border-color:rgba(0,0,0,0.2);--vsa-border-width:1px;--vsa-border-style:solid;--vsa-border:var(--vsa-border-width) var(--vsa-border-style) var(--vsa-border-color);--vsa-content-padding:1rem 1rem;--vsa-default-icon-size:1;display:block;max-width:var(--vsa-max-width);min-width:var(--vsa-min-width);width:100%;padding:0;margin:0;list-style:none;border:var(--vsa-border);color:var(--vsa-text-color);background-color:var(--vsa-bg-color)}.vsa-list [hidden]{display:none}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 92 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutAccordion_vue_vue_type_style_index_0_id_310676de_prod_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".list{max-width:100%;transition:all 2s ease-in-out}.vsa-item--is-active .vsa-item__heading,.vsa-item:not(:last-of-type){border-bottom:none;transition:all 2s ease-in-out}.vsa-item__content{opacity:1;padding-top:0;font-style:normal;font-weight:400;font-size:16px;line-height:24px;color:#171717;background-color:#f4f4f4}.vsa-item__trigger__content{text-align:start;font-weight:600;font-size:32px;color:#171717}.vsa-item__trigger:focus,.vsa-item__trigger:hover{outline:none!important;background-color:#f4f4f4;color:#000!important}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:before{left:32px!important}.vsa-item__trigger[aria-expanded=true] .vsa-item__trigger__icon--is-default:after{left:27px!important}.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:focus .vsa-item__trigger__icon--is-default:before,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:after,.vsa-item__trigger:hover .vsa-item__trigger__icon--is-default:before{background-color:#000!important}.vsa-item__trigger__icon--is-default:before{left:31px!important}.vsa-item__trigger{align-items:flex-start;padding:25px 30px!important}.vsa-item__trigger__icon--is-default{min-width:40px!important}.vsa-item__trigger__icon--is-default:after,.vsa-item__trigger__icon--is-default:before{width:15px}@media screen and (max-width:1024px){.vsa-item__trigger{align-items:flex-start;padding:20px 25px!important}.vsa-item__trigger__content{font-size:26px}.vsa-item__content{padding:0 25px}}@media screen and (max-width:768px){.vsa-item__trigger{align-items:flex-start;padding:15px 20px!important}.vsa-item__trigger__content{font-size:20px}.vsa-item__content{padding:0 20px}}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AuthorCard.vue?vue&type=template&id=ab593b50&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "w-full h-285 border border-white-secondary border-t-2 border-t-yellow-secondary relative"
  }, [_vm._ssrNode("<img" + _vm._ssrAttr("src", __webpack_require__(61)) + " alt=\"author\" class=\"w-full h-full object-cover object-center\"> <div class=\"py-30 px-15 absolute w-full bottom-0 left-0 ring-0 bg text-white-primary\"><h2 class=\"font-bold text-lg mb-5\">Bobir Radjabov</h2> <p class=\"text-xs\">Interyer dizayner</p></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AuthorCard.vue?vue&type=template&id=ab593b50&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AuthorCard.vue?vue&type=script&lang=js&
/* harmony default export */ var AuthorCardvue_type_script_lang_js_ = ({
  name: "AuthorCard"
});
// CONCATENATED MODULE: ./components/AuthorCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AuthorCardvue_type_script_lang_js_ = (AuthorCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AuthorCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(81)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AuthorCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "c025766a"
  
)

/* harmony default export */ var AuthorCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 102 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AboutAccordion.vue?vue&type=template&id=310676de&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('vsa-list', {
    staticClass: "list"
  }, [_c('vsa-item', [_c('vsa-heading', [_vm._v("How Much Does it Cost to Make an App?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1), _vm._v(" "), _c('vsa-item', [_c('vsa-heading', [_vm._v("What are the main steps to building an app?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1), _vm._v(" "), _c('vsa-item', [_c('vsa-heading', [_vm._v("What is the main difference between a website and a web\n        app?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1), _vm._v(" "), _c('vsa-item', [_c('vsa-heading', [_vm._v("What Is Mobile App Development Services?")]), _vm._v(" "), _c('vsa-content', [_vm._v("“Not all apps are created equal” - says the ‘Declaration of\n        Techdependence’. The type of web and mobile app will determine the\n        complexity of the solution and the number of hours required for its\n        development process. In IT the industry standard on paying people is\n        measured in hourly rates cost of which varies anywhere from $20 to\n        $200/hour.")])], 1)], 1);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AboutAccordion.vue?vue&type=template&id=310676de&

// EXTERNAL MODULE: external "vue-simple-accordion"
var external_vue_simple_accordion_ = __webpack_require__(41);

// EXTERNAL MODULE: ./node_modules/vue-simple-accordion/dist/vue-simple-accordion.css
var vue_simple_accordion = __webpack_require__(90);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AboutAccordion.vue?vue&type=script&lang=js&


/* harmony default export */ var AboutAccordionvue_type_script_lang_js_ = ({
  name: 'AboutAccordionCard',
  components: {
    VsaList: external_vue_simple_accordion_["VsaList"],
    VsaItem: external_vue_simple_accordion_["VsaItem"],
    VsaHeading: external_vue_simple_accordion_["VsaHeading"],
    VsaContent: external_vue_simple_accordion_["VsaContent"],
    VsaIcon: external_vue_simple_accordion_["VsaIcon"]
  }
});
// CONCATENATED MODULE: ./components/AboutAccordion.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AboutAccordionvue_type_script_lang_js_ = (AboutAccordionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AboutAccordion.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(92)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AboutAccordionvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "27cee5cb"
  
)

/* harmony default export */ var AboutAccordion = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 103 */,
/* 104 */,
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(110);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("00105d8a", content, true, context)
};

/***/ }),
/* 106 */,
/* 107 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AuthorDescCard.vue?vue&type=template&id=6053e95a&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "w-292 border border-white-secondary border-t-2 border-t-yellow-secondary relative"
  }, [_vm._ssrNode("<div class=\"p-30 relative box\" data-v-6053e95a><p class=\"text\" data-v-6053e95a>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper pellentesque eget venenatis, est urna. Congue augue risus eu, egestas aliquam lacus morbi. Duis amet nec eu turpis ut pharetra a facilisi. Purus morbi ornare sollicitudin in porttitor sit nerus mentos sollicitudin in porttitor sit nerus mentos.</p></div> <div class=\"p-30 w-full flex items-center gap-15 bg-gray-primary\" data-v-6053e95a><div class=\"w-50 h-50 rounded-full overflow-hidden border border-white-secondary\" data-v-6053e95a><img" + _vm._ssrAttr("src", __webpack_require__(61)) + " alt=\"author\" class=\"w-full h-full object-cover object-top\" data-v-6053e95a></div> <p class=\"text-base font-semibold flex-wrap leading-24\" data-v-6053e95a>Sardorbek Rashidov</p></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/AuthorDescCard.vue?vue&type=template&id=6053e95a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/AuthorDescCard.vue?vue&type=script&lang=js&
/* harmony default export */ var AuthorDescCardvue_type_script_lang_js_ = ({
  name: "AuthorDescCard"
});
// CONCATENATED MODULE: ./components/AuthorDescCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_AuthorDescCardvue_type_script_lang_js_ = (AuthorDescCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/AuthorDescCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(83)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_AuthorDescCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6053e95a",
  "e0bd8608"
  
)

/* harmony default export */ var AuthorDescCard = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 108 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/QuestionAccordion.vue?vue&type=template&id=5bc7ee2a&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "accordion accordion-flushborder border-gray-primary rounded-2px",
    attrs: {
      "id": "accordionFlushExample"
    }
  }, [_vm._ssrNode("<div class=\"accordion-item\" data-v-5bc7ee2a><h2 id=\"flush-headingOne\" class=\"accordion-header font-semibold text-lg md:text-xl lg:text-xxl\" data-v-5bc7ee2a><button type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseOne\" aria-expanded=\"false\" aria-controls=\"flush-collapseOne\" class=\"accordion-button font-semibold text-lg md:text-xl lg:text-xxl collapsed\" data-v-5bc7ee2a>\n                Accordion Item #1\n            </button></h2> <div id=\"flush-collapseOne\" aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\" class=\"accordion-collapse collapse\" data-v-5bc7ee2a><div class=\"accordion-body pt-0\" data-v-5bc7ee2a>Placeholder content for this accordion, which is intended to demonstrate the <code data-v-5bc7ee2a>.accordion-flush</code> class. This is the first item's accordion body.</div></div></div> <div class=\"accordion-item\" data-v-5bc7ee2a><h2 id=\"flush-headingTwo\" class=\"accordion-header font-semibold text-lg md:text-xl lg:text-xxl\" data-v-5bc7ee2a><button type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseTwo\" aria-expanded=\"false\" aria-controls=\"flush-collapseTwo\" class=\"accordion-button font-semibold text-lg md:text-xl lg:text-xxl collapsed\" data-v-5bc7ee2a>\n                Accordion Item #2\n            </button></h2> <div id=\"flush-collapseTwo\" aria-labelledby=\"flush-headingTwo\" data-bs-parent=\"#accordionFlushExample\" class=\"accordion-collapse collapse\" data-v-5bc7ee2a><div class=\"accordion-body pt-0\" data-v-5bc7ee2a>Placeholder content for this accordion, which is intended to demonstrate the <code data-v-5bc7ee2a>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div></div></div> <div class=\"accordion-item\" data-v-5bc7ee2a><h2 id=\"flush-headingThree\" class=\"accordion-header font-semibold text-lg md:text-xl lg:text-xxl\" data-v-5bc7ee2a><button type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseThree\" aria-expanded=\"false\" aria-controls=\"flush-collapseThree\" class=\"accordion-button font-semibold text-lg md:text-xl lg:text-xxl collapsed\" data-v-5bc7ee2a>\n                Accordion Item #3\n            </button></h2> <div id=\"flush-collapseThree\" aria-labelledby=\"flush-headingThree\" data-bs-parent=\"#accordionFlushExample\" class=\"accordion-collapse collapse\" data-v-5bc7ee2a><div class=\"accordion-body pt-0\" data-v-5bc7ee2a>Placeholder content for this accordion, which is intended to demonstrate the <code data-v-5bc7ee2a>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div></div></div>")]);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./components/QuestionAccordion.vue?vue&type=template&id=5bc7ee2a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/QuestionAccordion.vue?vue&type=script&lang=js&
/* harmony default export */ var QuestionAccordionvue_type_script_lang_js_ = ({
  name: "QuestionAccordion",
  head: {
    link: [{
      rel: "stylesheet",
      href: "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css",
      integrity: "sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC",
      crossorigin: "anonymous"
    }],
    script: [{
      src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js'
    }]
  }
});
// CONCATENATED MODULE: ./components/QuestionAccordion.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_QuestionAccordionvue_type_script_lang_js_ = (QuestionAccordionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/QuestionAccordion.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(87)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_QuestionAccordionvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "5bc7ee2a",
  "7e627bbc"
  
)

/* harmony default export */ var QuestionAccordion = __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 109 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_about_vue_vue_type_style_index_0_id_1133228c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(105);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_about_vue_vue_type_style_index_0_id_1133228c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_about_vue_vue_type_style_index_0_id_1133228c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_about_vue_vue_type_style_index_0_id_1133228c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_about_vue_vue_type_style_index_0_id_1133228c_prod_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".carousel-wrapper[data-v-1133228c]{padding-bottom:30px;width:100%;display:flex;overflow-x:scroll}.carousel-wrapper[data-v-1133228c]::-webkit-scrollbar{height:4px;background:#f4f4f4}.carousel-wrapper[data-v-1133228c]::-webkit-scrollbar-thumb{height:4px;width:100px!important;background:#ba8600}.swiper[data-v-1133228c]{width:100%;height:100%}.swiper-slide[data-v-1133228c]{text-align:center;display:flex;justify-content:center;align-items:center}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/about.vue?vue&type=template&id=1133228c&scoped=true&
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c('div', {
    staticClass: "bg-white-secondary relative space-y-60 pb-120"
  }, [_c('MainComponent', {
    attrs: {
      "page": "About us",
      "width": "max-w-500",
      "title": "<span class='text-yellow-primary font-mak'>Full</span> nformation about <span class='text-yellow-primary font-mak'>our team</span>"
    }
  }), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"container relative\" data-v-1133228c>", "</div>", [_vm._ssrNode("<div class=\"bg-white-primary p-30 md:p-48 lg:p-60 flex flex-col gap-60 relative\" data-v-1133228c>", "</div>", [_vm._ssrNode("<p class=\"absoluteText absolute top-100 left-0 break-all uppercase text-BgTitle md:text-mdBgTitle lg:text-lgBgTitle font-mak font-bold text-gray-primary\" data-v-1133228c>Architecture</p> <h2 class=\"font-medium text-Title md:text-mdTitle lg:text-lgTitle uppercase z-10\" data-v-1133228c>a brief illustrated title on Architecture</h2> <div class=\"flex flex-col md:flex-row gap-15 z-10\" data-v-1133228c><div class=\"w-54 h-54 shrink-0 rounded-full border border-white-secondary flex items-center justify-center\" data-v-1133228c><img" + _vm._ssrAttr("src", __webpack_require__(55)) + " width=\"24\" height=\"24\" alt=\"about\" class=\"object-cover object-center\" data-v-1133228c></div> <p class=\"text-base leading-24\" data-v-1133228c>Architecture is the art and science of enhancing the interior of a building to achieve a healthier and more aesthetically pleasing environment for the people using the space. An interior designer is someone who plans, researches, coordinates, and manages such enhancement projects. Interior design is a multifaceted profession that includes conceptual development, space planning, site inspections, programming, research, communicating with the stakeholders of a project, construction management, and execution of the design.</p></div> "), _vm._ssrNode("<div class=\"grid md:grid-cols-2 lg:grid-cols-3 gap-30\" data-v-1133228c>", "</div>", [_c('StatisticCard', {
    attrs: {
      "color": "bg-gray-primary text-black-primary",
      "bgColor": "statistics__item2"
    }
  }), _vm._ssrNode(" "), _c('StatisticCard', {
    attrs: {
      "color": "bg-gray-primary text-black-primary",
      "bgColor": "statistics__item2"
    }
  }), _vm._ssrNode(" "), _c('StatisticCard', {
    attrs: {
      "color": "bg-gray-primary text-black-primary",
      "bgColor": "statistics__item2"
    }
  })], 2), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-15 py-60\" data-v-1133228c>", "</div>", [_c('AuthorCard'), _vm._ssrNode(" "), _c('AuthorCard'), _vm._ssrNode(" "), _c('AuthorCard'), _vm._ssrNode(" "), _c('AuthorCard')], 2), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"carousel-wrapper\" data-v-1133228c>", "</div>", [_vm._ssrNode("<div class=\"flex gap-15 pr-2\" data-v-1133228c>", "</div>", [_c('AuthorDesCard'), _vm._ssrNode(" "), _c('AuthorDesCard'), _vm._ssrNode(" "), _c('AuthorDesCard'), _vm._ssrNode(" "), _c('AuthorDesCard'), _vm._ssrNode(" "), _c('AuthorDesCard'), _vm._ssrNode(" "), _c('AuthorDesCard')], 2)]), _vm._ssrNode(" "), _c('AboutAccordion')], 2)])], 2);
};

var staticRenderFns = [];

// CONCATENATED MODULE: ./pages/about.vue?vue&type=template&id=1133228c&scoped=true&

// EXTERNAL MODULE: ./components/AuthorCard.vue + 4 modules
var AuthorCard = __webpack_require__(101);

// EXTERNAL MODULE: ./components/ArticleCard.vue + 4 modules
var ArticleCard = __webpack_require__(80);

// EXTERNAL MODULE: ./components/MainComponent.vue + 4 modules
var MainComponent = __webpack_require__(49);

// EXTERNAL MODULE: ./components/AuthorDescCard.vue + 4 modules
var AuthorDescCard = __webpack_require__(107);

// EXTERNAL MODULE: ./components/StatisticCard.vue + 4 modules
var StatisticCard = __webpack_require__(62);

// EXTERNAL MODULE: ./components/QuestionAccordion.vue + 4 modules
var QuestionAccordion = __webpack_require__(108);

// EXTERNAL MODULE: ./components/TheHeader.vue + 4 modules
var TheHeader = __webpack_require__(7);

// EXTERNAL MODULE: ./components/AboutAccordion.vue + 4 modules
var AboutAccordion = __webpack_require__(102);

// CONCATENATED MODULE: ./plugins/main.js
/* harmony default export */ var main = (() => {
  letswiper = new Swiper('.mySwiper', {
    freeMode: true,
    pagination: {
      clickable: true
    },
    centeredSlides: false,
    slidesPerGroupSkip: 1,
    grabCursor: false,
    keyboard: {
      enabled: true
    },
    spaceBetween: 15,
    loop: true,
    loopFillGroupWithBlank: false,
    slidesPerView: 4,
    slidesPerGroup: 4,
    breakpoints: {
      1090: {
        slidesPerView: 3,
        slidesPerGroup: 3
      },
      920: {
        slidesPerView: 2,
        slidesPerGroup: 2
      },
      750: {
        slidesPerView: 1,
        slidesPerGroup: 1
      }
    },
    scrollbar: {
      el: '.swiper-scrollbar' // hide: true,

    }
  });
});
// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/about.vue?vue&type=script&lang=js&









/* harmony default export */ var aboutvue_type_script_lang_js_ = ({
  name: 'AboutPage',
  components: {
    AuthorCard: AuthorCard["default"],
    ArticleCard: ArticleCard["default"],
    AuthorDesCard: AuthorDescCard["default"],
    MainComponent: MainComponent["default"],
    StatisticCard: StatisticCard["default"],
    QuestionAccordion: QuestionAccordion["default"],
    TheHeader: TheHeader["default"],
    AboutAccordion: AboutAccordion["default"]
  },
  head: {
    link: [{
      rel: 'stylesheet',
      href: 'https://unpkg.com/swiper@8/swiper-bundle.min.css'
    }],
    script: [{
      src: 'https://unpkg.com/swiper@8/swiper-bundle.min.js'
    }]
  }
});
// CONCATENATED MODULE: ./pages/about.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_aboutvue_type_script_lang_js_ = (aboutvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./pages/about.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(109)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_aboutvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "1133228c",
  "78596219"
  
)

/* harmony default export */ var about = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {MainComponent: __webpack_require__(49).default,StatisticCard: __webpack_require__(62).default,AuthorCard: __webpack_require__(101).default,AboutAccordion: __webpack_require__(102).default})


/***/ })
]);;
//# sourceMappingURL=about.js.map