# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AboutAccordion>` | `<about-accordion>` (components/AboutAccordion.vue)
- `<AboutContainer>` | `<about-container>` (components/AboutContainer.vue)
- `<AdvantageCard>` | `<advantage-card>` (components/AdvantageCard.vue)
- `<ArticleCard>` | `<article-card>` (components/ArticleCard.vue)
- `<AuthorCard>` | `<author-card>` (components/AuthorCard.vue)
- `<AuthorDescCard>` | `<author-desc-card>` (components/AuthorDescCard.vue)
- `<ExampleCard>` | `<example-card>` (components/ExampleCard.vue)
- `<MainComponent>` | `<main-component>` (components/MainComponent.vue)
- `<Map>` | `<map>` (components/Map.vue)
- `<QuestionAccordion>` | `<question-accordion>` (components/QuestionAccordion.vue)
- `<ScrollLoop>` | `<scroll-loop>` (components/ScrollLoop.vue)
- `<ServicesCard>` | `<services-card>` (components/ServicesCard.vue)
- `<StatisticCard>` | `<statistic-card>` (components/StatisticCard.vue)
- `<SwiperNext>` | `<swiper-next>` (components/SwiperNext.vue)
- `<TheFooter>` | `<the-footer>` (components/TheFooter.vue)
- `<TheHeader>` | `<the-header>` (components/TheHeader.vue)
