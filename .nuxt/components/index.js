export const AboutAccordion = () => import('../../components/AboutAccordion.vue' /* webpackChunkName: "components/about-accordion" */).then(c => wrapFunctional(c.default || c))
export const AboutContainer = () => import('../../components/AboutContainer.vue' /* webpackChunkName: "components/about-container" */).then(c => wrapFunctional(c.default || c))
export const AdvantageCard = () => import('../../components/AdvantageCard.vue' /* webpackChunkName: "components/advantage-card" */).then(c => wrapFunctional(c.default || c))
export const ArticleCard = () => import('../../components/ArticleCard.vue' /* webpackChunkName: "components/article-card" */).then(c => wrapFunctional(c.default || c))
export const AuthorCard = () => import('../../components/AuthorCard.vue' /* webpackChunkName: "components/author-card" */).then(c => wrapFunctional(c.default || c))
export const AuthorDescCard = () => import('../../components/AuthorDescCard.vue' /* webpackChunkName: "components/author-desc-card" */).then(c => wrapFunctional(c.default || c))
export const ExampleCard = () => import('../../components/ExampleCard.vue' /* webpackChunkName: "components/example-card" */).then(c => wrapFunctional(c.default || c))
export const MainComponent = () => import('../../components/MainComponent.vue' /* webpackChunkName: "components/main-component" */).then(c => wrapFunctional(c.default || c))
export const Map = () => import('../../components/Map.vue' /* webpackChunkName: "components/map" */).then(c => wrapFunctional(c.default || c))
export const QuestionAccordion = () => import('../../components/QuestionAccordion.vue' /* webpackChunkName: "components/question-accordion" */).then(c => wrapFunctional(c.default || c))
export const ScrollLoop = () => import('../../components/ScrollLoop.vue' /* webpackChunkName: "components/scroll-loop" */).then(c => wrapFunctional(c.default || c))
export const ServicesCard = () => import('../../components/ServicesCard.vue' /* webpackChunkName: "components/services-card" */).then(c => wrapFunctional(c.default || c))
export const StatisticCard = () => import('../../components/StatisticCard.vue' /* webpackChunkName: "components/statistic-card" */).then(c => wrapFunctional(c.default || c))
export const SwiperNext = () => import('../../components/SwiperNext.vue' /* webpackChunkName: "components/swiper-next" */).then(c => wrapFunctional(c.default || c))
export const TheFooter = () => import('../../components/TheFooter.vue' /* webpackChunkName: "components/the-footer" */).then(c => wrapFunctional(c.default || c))
export const TheHeader = () => import('../../components/TheHeader.vue' /* webpackChunkName: "components/the-header" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
